#!/usr/bin/env python

import re, argparse, sys, os

parser = argparse.ArgumentParser(description ='read file alignlog.txt and print various infromations')
parser.add_argument('-f','--input',help='input file', default = os.getcwd())
args = parser.parse_args()


def findFile(file, startingPath):
    # find alignlog.txt by looking in the subfolders
    file_path = 0
    if args.input.split('/')[-1] == file:
        file_path = startingPath
    else:
        for root, dirs, files in os.walk(startingPath):
            if file in files and 'Iter' not in root:
                file_path = root+'/'+file
    if not file_path:
        print file+' not found'
        sys.exit(1)
    return file_path

file_name = findFile('alignlog.txt', args.input)

file_content = open(file_name).read()

regex = {}
keys = ('numEvents', 'numVertices', 'numTracks', 'chi2Vertices', 'chi2Tracks', 'chi2Tot', 'isConverged')
titles = {'numEvents' : '# events',
          'numVertices' : '# vertices',
          'numTracks' : '# tracks',
          'chi2Vertices' : 'chi2/dof vertices',
          'chi2Tracks' : 'chi2/dof tracks',
          'chi2Tot' : 'total chi2/dof',
          'isConverged' : 'Convergence status',
          }

regex['numEvents'] = re.compile(r'Total number of events: ([0-9.+e]*)')
regex['numVertices'] = re.compile(r'Used ([0-9.+e]*) vertices for alignment')
regex['numTracks'] = re.compile(r'Used ([0-9.+e]*) tracks for alignment')
regex['chi2Vertices'] = re.compile('Vertex chisquare/dof: *([0-9.+e]*\.?[0-9.+e]*)')
regex['chi2Tracks'] = re.compile('Track chisquare/dof: *([0-9.+e]*\.?[0-9.+e]*)')
regex['chi2Tot'] = re.compile('Total chisquare/dofs: *([0-9.+e]*\.?[0-9.+e]* / [0-9.+e]*)')
regex['isConverged'] = re.compile('Convergence status: *([0-9.+e]*)')

# for line in inFile:
#     if re_numVertices.match(line):
#         print line

values = {}
for key in keys:
    values[key] = regex[key].findall(file_content)
    values[key].insert(0,titles[key])

maxLen = max([len(i) for i in sum(values.values(),[])])

#print values

for i in range(len(values['numTracks'])):
    print ((' {:^'+str(maxLen)+'} ')*len(values)).format(*[values[key][i] for key in keys])
