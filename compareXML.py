#!/usr/bin/env python

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make plots to compare xmls")
    parser.add_argument('files',help='''files to process
    es label1:directory1  label2:directory2 I assume that the files Global.xml, Detectors.xml and Modules.xml are in the directories''', nargs='+', type=str)
    parser.add_argument('-o','--outFile',help='output file name do not put the extension because it will create 3 files',default='plotsXML')
    parser.add_argument('-y','--fixRangeY',help='fix y-range to default values',action='store_true')
    parser.add_argument('-n','--numSens',help='Use sensor number instead of z-position',action='store_true')
    parser.add_argument('-d','--plotsDir',help='create folder with a pdf for each plot',nargs='?',const='plotsXML')
    parser.add_argument('--noStd',help='In diff plots do not draw first one',action='store_true')
    parser.add_argument('--oldDB',help='Have Modules and Detectors in separate files',action='store_true')
    args = parser.parse_args()

##########################
    
import sys, os, fnmatch
from sensorUtils import *

def findPaths(file_names, startingPath):
    try:
        for file_name in file_names:
            for root, dirs, files in os.walk(startingPath):
                for file in files:
                    if fnmatch.fnmatch(file, file_name):
                        return root+'/'+file
    except TypeError:
        file_name = file_names
        for root, dirs, files in os.walk(startingPath):
            for file in files:
                if fnmatch.fnmatch(file, file_name):
                    return root+'/'+file

def compareGlobal(list_files, outFile_name=None, outDir=None):
    if outFile_name:
        outFile = open(outFile_name, 'w')
    else:
        outFile = sys.stdout
    alignments = {}
    for key, file in list_files:
        alignments[key] = GlobalMis(findPaths(['Global.xml', 'VeloGlobal.xml'],file))
    alignments_diff = {key: alignments[key]-alignments[list_files[0][0]] for key in alignments}
    
    # print type(alignments[list_files[0][0]]),  type(alignments_diff[list_files[0][0]])
    # print alignments[list_files[0][0]]['VeloSystem']
    # print alignments_diff[list_files[0][0]]['VeloSystem']
            
    maxLen    = 15
    precision = 6
    for mis, unita in (('dx', 'um'), ('dy', 'um'), ('dz', 'um'), ('dalpha', 'mrad'), ('dbeta', 'mrad'), ('dgamma', 'mrad')):
        outFile.write( ('{:^'+str(maxLen)+'}{:*^'+str(maxLen*3)+'}').format('',' '+mis+' ['+unita+'] ')+'\n' )
        outFile.write( (('{:^'+str(maxLen)+'}')*4).format('','VeloSystem', 'VeloLeft', 'VeloRight')+'\n' )
        for key, file in list_files:
            outFile.write( ('{:<'+str(maxLen)+'}'+('{:^'+str(maxLen)+'.'+str(precision)+'}')*3).format(key,float(getattr(alignments[key]['VeloSystem'],mis)), float(getattr(alignments[key]['VeloLeft'],mis)), float(getattr(alignments[key]['VeloRight'],mis)))+'\n' )
        #outFile.write('\n')
        outFile.write( ('{:^'+str(maxLen)+'}{:-^'+str(maxLen*3)+'}').format('',' diff wrt '+list_files[0][0]+' ')+'\n')
        for key, file in list_files:
            outFile.write( ('{:<'+str(maxLen)+'}'+('{:^'+str(maxLen)+'.'+str(precision)+'}')*3).format(key,float(getattr(alignments_diff[key]['VeloSystem'],mis)),float( getattr(alignments_diff[key]['VeloLeft'],mis)), float(getattr(alignments_diff[key]['VeloRight'],mis)))+'\n' )
        outFile.write('\n')
    if outFile_name: outFile.close()    

    # Print also plots
    if args.noStd: del alignments_diff[list_files[0][0]]
    if outFile_name:
        outFile_name = os.path.splitext(outFile_name)[0]+'.pdf'
        
        sensAlignments = {key:align.getSensMisCollection() for key, align in alignments.items()}
        sensAlignments_diff = {key:align.getSensMisCollection() for key, align in alignments_diff.items()}

        if outDir != None:
            outDir = outDir+'/Global'
            os.system('mkdir -p '+outDir)
            c1 = TCanvas('c1', 'c1')
            c1.SetGrid()
        c = TCanvas('c', 'c')
        c.Print(outFile_name+'[')
        c.Divide(1,2)
        for i in range(3):
            c.GetPad(i).SetGrid()

        rangeY_dict = {'dx':(-200,200), 'dy':(-200,200), 'dz':(-200,200), 'dalpha':(-10,10), 'dbeta':(-10,10), 'dgamma':(-2,2)} if args.fixRangeY else {}
        hs = drawGraphsMis(sensAlignments, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', rangeY_dict=rangeY_dict, isZPos=isZPos)
        for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
            c.cd(1)
            hs[paio[0]].Draw()
            if outDir != None:
                c1.cd()
                hs[paio[0]].Draw()
                c1.Update()
                c1.Print(outDir+'/'+paio[0]+'.pdf')
            c.cd(2)
            hs[paio[1]].Draw()
            c.Update()
            c.Print(outFile_name)
            if outDir != None:
                c1.cd()
                hs[paio[1]].Draw()
                c1.Update()
                c1.Print(outDir+'/'+paio[1]+'.pdf')
            

        rangeY_dict = {'dx':(-20,20), 'dy':(-20,20), 'dz':(-10,10), 'dalpha':(-0.1,0.1), 'dbeta':(-0.1,0.1), 'dgamma':(-0.3,0.3)} if args.fixRangeY else {}
        hs = drawGraphsMis(sensAlignments_diff, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', title='diff wrt '+list_files[0][0]+' ', rangeY_dict=rangeY_dict, isZPos=isZPos)
        for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
            c.cd(1)
            hs[paio[0]].Draw()
            if outDir != None:
                c1.cd()
                hs[paio[0]].Draw()
                c1.Update()
                c1.Print(outDir+'/'+paio[0]+'_diff.pdf')
            c.cd(2)
            hs[paio[1]].Draw()
            c.Update()
            c.Print(outFile_name)
            if outDir != None:
                c1.cd()
                hs[paio[1]].Draw()
                c1.Update()
                c1.Print(outDir+'/'+paio[1]+'_diff.pdf')

        c.Print(outFile_name+']')
        return sensAlignments   

def compareModules(list_files, outFile_name, outDir=None):
    alignments = {}
    for key, file in list_files:
        alignments[key] = SensMisCollection()
        alignments[key].readFromXML(findPaths(['Modules.xml','VeloModules.xml'],file), 'module')
    alignments_diff = {key: alignments[key]-alignments[list_files[0][0]] for key in alignments}
    if args.noStd: del alignments_diff[list_files[0][0]]

    if outDir != None:
        outDir = outDir+'/Modules'
        os.system('mkdir -p '+outDir)
        c1 = TCanvas('c1', 'c1')
        c1.SetGrid()
    c = TCanvas('c', 'c')
    c.Print(outFile_name+'[')
    c.Divide(1,2)
    for i in range(3):
        c.GetPad(i).SetGrid()
    
    # for key, file  in list_files:
    #     hs = drawGraphsMis({key:alignments[key]}, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r')
    #     for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
    #         c.cd(1)
    #         hs[paio[0]].Draw()
    #         c.cd(2)
    #         hs[paio[1]].Draw()
    #         c.Update()
    #         c.Print(outFile_name)
    
    rangeY_dict = {'dx':(-120,120), 'dy':(-120,120), 'dz':(-600,600), 'dalpha':(-10,10), 'dbeta':(-10,10), 'dgamma':(-2,2)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if outDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if outDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'.pdf')

    #rangeY_dict = {'dx':(-50,50), 'dy':(-50,50), 'dz':(-70,70), 'dalpha':(-5,5), 'dbeta':(-5,5), 'dgamma':(-0.2,0.2)} if args.fixRangeY else {}
    rangeY_dict = {'dx':(-10,10), 'dy':(-10,10), 'dz':(-25,25), 'dalpha':(-5,5), 'dbeta':(-5,5), 'dgamma':(-0.2,0.2)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments_diff, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', title='diff wrt '+list_files[0][0]+' ', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if outDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'_diff.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if outDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'_diff.pdf')

    c.Print(outFile_name+']')

    for key, misCollection in alignments_diff.items():
        print key
        for mis in ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma']:
            print mis, sum([getattr(misCollection[i], mis) for i in range(42) if i%2==0])
    
    return alignments


def compareSensors(list_files, outFile_name, outDir=None):
    alignments = {}
    for key, file in list_files:
        alignments[key] = SensMisCollection()
        sensFile_name = ['Detectors.xml', 'VeloDetectors.xml'] if args.oldDB else ['Modules.xml', 'VeloModules.xml']
        alignments[key].readFromXML(findPaths(sensFile_name, file), 'sensor')
    alignments_diff = {key: alignments[key]-alignments[list_files[0][0]] for key in alignments}
    if args.noStd: del alignments_diff[list_files[0][0]]

    #for num in range(42):#(43,106):
        #print num,alignments['Iter0'][num].dz
    if outDir != None:
        outDir = outDir+'/Sensors'
        os.system('mkdir -p '+outDir)
        c1 = TCanvas('c1', 'c1')
        c1.SetGrid()
    c = TCanvas('c', 'c')
    #c.SetGrid()
    c.Print(outFile_name+'[')
    c.Divide(1,2)
    for i in range(3):
        c.GetPad(i).SetGrid()
    
    # for key, file  in list_files:
    #     hs = drawGraphsMis({key:alignments[key]}, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='both', show_dy='both', show_dz='both', show_dalpha='both', show_dbeta='both', show_dgamma='both')
    #     for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
    #         c.cd(1)
    #         hs[paio[0]].Draw()
    #         c.cd(2)
    #         hs[paio[1]].Draw()
    #         c.Update()
    #         c.Print(outFile_name)

    rangeY_dict = {'dx':(-20,20), 'dy':(-20,20), 'dz':(-350,350), 'dalpha':(-20,20), 'dbeta':(-20,20), 'dgamma':(-20,20)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='phi', show_dy='phi', show_dz='phi', show_dalpha='phi', show_dbeta='phi', show_dgamma='phi', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if outDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if outDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'.pdf')

    rangeY_dict = {'dx':(-10,10), 'dy':(-10,10), 'dz':(-3,3), 'dalpha':(-0.1,0.1), 'dbeta':(-0.1,0.1), 'dgamma':(-0.01,0.01)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments_diff, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='phi', show_dy='phi', show_dz='phi', show_dalpha='phi', show_dbeta='phi', show_dgamma='phi', title='diff wrt '+list_files[0][0]+' ', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if outDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'_diff.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if outDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'_diff.pdf')

    c.Print(outFile_name+']')
    return alignments
    



if __name__ == '__main__':

    import ROOT as r
    r.gROOT.SetBatch(True)
    this_files_dir = os.path.dirname(os.path.realpath(__file__))
    r.gROOT.ProcessLine('.L '+os.path.join(this_files_dir, 'LHCbStyle.C'))
    r.gStyle.SetGridColor(16)

    isZPos = not args.numSens

    list_files = [[i.split(':')[0], os.path.normpath(os.path.expandvars(os.path.expanduser(i.split(':')[1])))] for i in args.files]#[i.split(':') for i in args.files]
    #list_files = [('Iter0', '/afs/cern.ch/user/g/gdujany/LHCb/Alignment/align2011/newAlignment/align2halves/AlignmentResults/Iter0/'), ('Iter1', '/afs/cern.ch/user/g/gdujany/LHCb/Alignment/align2011/newAlignment/align2halves/AlignmentResults/Iter1/')]

    glob = compareGlobal(list_files, args.outFile+'_global.txt',outDir=args.plotsDir)
    mod  = compareModules(list_files, args.outFile+'_modules.pdf',outDir=args.plotsDir)
    sens = compareSensors(list_files, args.outFile+'_sensors.pdf',outDir=args.plotsDir)

    # Print also total misalignment
    #alignments = {key: glob[key]+mod[key]+sens[key] for key in glob}
    alignments = {key: glob[key]+mod[key] for key in glob}
    alignments_diff = {key: alignments[key]-alignments[list_files[0][0]] for key in alignments}
    if args.noStd: del alignments_diff[list_files[0][0]]

    outFile_name = args.outFile+'_total.pdf'
    
    if args.plotsDir != None:
        outDir = args.plotsDir+'/Total'
        os.system('mkdir -p '+outDir)
        c1 = TCanvas('c1', 'c1')
        c1.SetGrid()
    c = TCanvas('c', 'c')
    c.Print(outFile_name+'[')
    c.Divide(1,2)
    for i in range(3):
        c.GetPad(i).SetGrid()
    
    rangeY_dict = {'dx':(-200,200), 'dy':(-200,200), 'dz':(-200,200), 'dalpha':(-10,10), 'dbeta':(-10,10), 'dgamma':(-2,2)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if args.plotsDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if args.plotsDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'.pdf')

    rangeY_dict = {'dx':(-10,10), 'dy':(-10,10), 'dz':(-25,25), 'dalpha':(-5,5), 'dbeta':(-5,5), 'dgamma':(-0.3,0.3)} if args.fixRangeY else {}
    hs = drawGraphsMis(alignments_diff, ['dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'], show_dx='r', show_dy='r', title='diff wrt '+list_files[0][0]+' ', rangeY_dict=rangeY_dict, isZPos=isZPos)
    for paio in (('dx', 'dy'), ('dz', 'dgamma'), ('dalpha', 'dbeta')):
        c.cd(1)
        hs[paio[0]].Draw()
        if args.plotsDir != None:
            c1.cd()
            hs[paio[0]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[0]+'_diff.pdf')
        c.cd(2)
        hs[paio[1]].Draw()
        c.Update()
        c.Print(outFile_name)
        if args.plotsDir != None:
            c1.cd()
            hs[paio[1]].Draw()
            c1.Update()
            c1.Print(outDir+'/'+paio[1]+'_diff.pdf')

    c.Print(outFile_name+']')
