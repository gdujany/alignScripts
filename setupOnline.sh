DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/pytools/1.9_python2.7/x86_64-slc6-gcc48-opt/bin:$PATH
export PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/Python/2.7.9.p1/x86_64-slc6-gcc48-opt/bin:$PATH
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/pytools/1.9_python2.7/x86_64-slc6-gcc48-opt/lib/python2.7/site-packages/:$LD_LIBRARY_PATH
export PYTHONPATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/pytools/1.9_python2.7/x86_64-slc6-gcc48-opt/lib/python2.7/site-packages/:$PYTHONPATH
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/pyanalysis/1.5_python2.7/x86_64-slc6-gcc48-opt/lib/python2.7/site-packages/:$LD_LIBRARY_PATH
export PYTHONPATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/pyanalysis/1.5_python2.7/x86_64-slc6-gcc48-opt/lib/python2.7/site-packages/:$PYTHONPATH
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/gcc/4.8.1/x86_64-slc6/setup.sh
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_76root6/ROOT/6.02.08/x86_64-slc6-gcc48-opt/bin/thisroot.sh
export PATH=$DIR:$PATH
export PYTHONPATH=$DIR:$PYTHONPATH
