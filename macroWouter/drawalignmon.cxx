#include <vector>
#include <TString.h>

MonitoringApplication* configureMonApp()
{
  MonitoringApplication* monapp = new MonitoringApplication() ;
  monapp->addPage(new MonitoringPage("tracks",
				     "Track/TrackMonitor/1",
				     "Track/TrackMonitor/2",
				     "Track/TrackMonitor/TrackMultiplicityFine",
				     "Track/TrackMonitor/history")) ;
  
  monapp->addPage(new MonitoringPage("Long track properties",
				     "Track/TrackMonitor/Long/5",
				     "Track/TrackMonitor/Long/126",
				     "Track/TrackMonitor/Long/125",
				     "Track/TrackMonitor/Long/6",
				     "Track/TrackMonitor/Long/7",
				     "Track/TrackMonitor/Long/8") ) ;
  
  monapp->addPage(new MonitoringPage("Downstream track properties",
				     "Track/TrackMonitor/Downstream/2",
				     "Track/TrackMonitor/Downstream/126",
				     "Track/TrackMonitor/Downstream/125",
				     "Track/TrackMonitor/Downstream/6",
				     "Track/TrackMonitor/Downstream/7",
				     "Track/TrackMonitor/Downstream/8") ) ;
  
  monapp->addPage(new MonitoringPage("Long track hit multiplicity",
				     "Track/TrackMonitor/Long/110",
				     "Track/TrackMonitor/Long/111",
				     "Track/TrackMonitor/Long/112",
				     "Track/TrackMonitor/Long/114",
				     "Track/TrackMonitor/Long/115",
				     "Track/TrackMonitor/Long/101") ) ;
  
  monapp->addPage(new MonitoringPage("Hits per Long track per layer",
				     "Track/TrackMonitor/Long/HitVeloLayers",
				     "Track/TrackMonitor/Long/HitTTLayers",
				     "Track/TrackMonitor/Long/HitOTLayers",
				     "Track/TrackMonitor/Long/HitITLayers",
				     "Track/TrackMonitor/Long/outliertype" ) ) ;
  
  monapp->addPage(new MonitoringPage("Hits per Downstream track (per layer)",
				     "Track/TrackMonitor/Downstream/110",
				     "Track/TrackMonitor/Downstream/111",
				     "Track/TrackMonitor/Downstream/112",
				     "Track/TrackMonitor/Downstream/HitTTLayers",
				     "Track/TrackMonitor/Downstream/HitOTLayers",
				     "Track/TrackMonitor/Downstream/HitITLayers") ) ;
  
  monapp->addPage(new MonitoringPage("Hits per Ttrack track (per layer)",
				     "Track/TrackMonitor/Ttrack/111",
				     "Track/TrackMonitor/Ttrack/112",
				     "Track/TrackMonitor/Ttrack/HitOTLayers",
				     "Track/TrackMonitor/Ttrack/HitITLayers") ) ;
  
  monapp->addPage(new MonitoringPage("long track chisquares",
				     "Track/TrackMonitor/Long/2",
				     "Track/TrackMonitor/Long/3",
				     "Track/TrackMonitor/Long/chi2PerDofMatch",
				     "Track/TrackMonitor/Long/chi2PerDofDownstream",
				     "Track/TrackMonitor/Long/chi2PerDofVelo",
				     "Track/TrackMonitor/Long/numiter")) ;
  
  monapp->addPage(new MonitoringPage("long track chisquare versus momentum, eta, phi",
				     "Track/TrackMonitor/Long/chi2ProbVsMom",
				     "Track/TrackMonitor/Long/chi2ProbVsEta",
				     "Track/TrackMonitor/Long/chi2ProbVsPhi",
				     "Track/TrackMonitor/Long/chi2ProbMatchVsMom",
				     "Track/TrackMonitor/Long/chi2ProbDownstreamVsMom",
				     "Track/TrackMonitor/Long/chi2ProbVeloVsMom") ) ;

  monapp->addPage(new MonitoringPage("long track chisquare for <10GeV (top) and >10GeV (bottom) momentum",
				     "Track/LowMomMonitor/Long/3",
				     "Track/LowMomMonitor/Long/chi2PerDofMatch",
				     "Track/LowMomMonitor/Long/chi2PerDofDownstream",
				     "Track/HighMomMonitor/Long/3",
				     "Track/HighMomMonitor/Long/chi2PerDofMatch",
				     "Track/HighMomMonitor/Long/chi2PerDofDownstream")) ;
  
 monapp->addPage(new MonitoringPage("velo track chisquares",
				    "Track/TrackMonitor/Velo/chi2PerDofVelo",
				    "Track/TrackMonitor/Velo/numiter",
				    "Track/TrackMonitor/Velo/ndof",
				    "Track/TrackMonitor/Velo/101",
				    "Track/TrackMonitor/Velo/VeloPhiresidualPull",
				     "Track/TrackMonitor/Velo/VeloRresidualPull")) ;
 monapp->addPage(new MonitoringPage("Velo tracks",
                                    "Track/TrackMonitor/Velo/7",
                                    "Track/TrackMonitor/Velo/8",
                                    "Track/TrackMonitor/Velo/114",
                                    "Track/TrackMonitor/Velo/115"));


 monapp->addPage(new MonitoringPage("Velo tracks pseudoefficiency",
                                    "Velo/VeloTrackMonitor/Track_pseudoEfficiency",
                                    "Velo/VeloTrackMonitor/Pseudoefficiency_per_sensor_vs_sensorID",
                                    "Velo/VeloTrackMonitor/UsedSensors"));

 monapp->addPage(new MonitoringPage("Velo tracks: first state",
                                    "Track/TrackMonitor/Velo/120",
                                    "Track/TrackMonitor/Velo/121",
                                    "Track/TrackMonitor/Velo/122",
                                    "Track/TrackMonitor/Velo/123",
                                    "Track/TrackMonitor/Velo/124",
                                    "Track/TrackMonitor/Velo/125"));
 
 monapp->addPage(new MonitoringPage("velo residual Long - Velo tracks",
                                    "Track/TrackMonitor/Long/VeloPhiResidual",
                                    "Track/TrackMonitor/Long/VeloRResidual",
                                    "Track/TrackMonitor/Long/VeloPhiResidualPull",
                                    "Track/TrackMonitor/Long/VeloRResidualPull",
                                    "Track/TrackMonitor/Velo/VeloPhiresidual",
                                    "Track/TrackMonitor/Velo/VeloRresidual",
                                    "Track/TrackMonitor/Velo/VeloPhiresidualPull",
                                    "Track/TrackMonitor/Velo/VeloRresidualPull"
                                    ));
 monapp->addPage(new MonitoringPage("residuals",
				     "Track/TrackMonitor/Long/OTResidual",
				     "Track/TrackMonitor/Long/ITResidual",
				     "Track/TrackMonitor/Long/TTResidual",
				     "Track/TrackMonitor/Long/VeloPhiResidual",
				     "Track/TrackMonitor/Long/VeloRResidual") ) ;

  monapp->addPage(new MonitoringPage("residuals for >10 GeV tracks",
				     "Track/HighMomMonitor/Long/OTResidual",
				     "Track/HighMomMonitor/Long/ITResidual",
				     "Track/HighMomMonitor/Long/TTResidual",
				     "Track/HighMomMonitor/Long/VeloPhiResidual",
				     "Track/HighMomMonitor/Long/VeloRResidual") ) ;

  monapp->addPage(new MonitoringPage("residual pulls",
				     "Track/TrackMonitor/Long/OTresidualPull",
				     "Track/TrackMonitor/Long/ITresidualPull",
				     "Track/TrackMonitor/Long/TTresidualPull",
				     "Track/TrackMonitor/Long/VeloPhiresidualPull",
				     "Track/TrackMonitor/Long/VeloRresidualPull",
				     "Track/TrackMonitor/Long/outliertype") ) ;

   monapp->addPage(new MonitoringPage("TT clusters on tracks",
				      "Track/TTTrackMonitor/charge",
				      "Track/TTTrackMonitor/cluster charge vs slope",
				      "Track/TTTrackMonitor/size",
				      "Track/TTTrackMonitor/cluster size vs tx") ) ;
   
   monapp->addPage(new MonitoringPage("IT1 clusters on tracks",
				      "Track/ITTrackMonitor/IT1/charge",
				      "Track/ITTrackMonitor/IT1/cluster charge vs slope",
				      "Track/ITTrackMonitor/IT1/size",
				      "Track/ITTrackMonitor/IT1/cluster size vs tx") ) ;
   
   monapp->addPage(new MonitoringPage("OT hits on tracks",
				      "OT/OTTrackMonitor/station1/drifttime",
				      "OT/OTHitEfficiencyMonitor/Module4/effvsdist",
				      "OT/OTTrackMonitor/avtimeres",
				      "OT/OTTrackMonitor/station1/residualgood",
				      "OT/OTTrackMonitor/station1/drifttimeresidualgood",
				      "OT/OTTrackMonitor/station1/avtimeresvsy")) ;

   monapp->addPage(new MonitoringPage("More OT hits on tracks",
				      "OT/OTTrackMonitor/drifttimeuse",
				      "OT/OTTrackMonitor/station1/trkdistgood",
				      "OT/OTTrackMonitor/station1/trkdist",
				      "OT/OTTrackMonitor/driftTimeUseVsTrkDist",
				      "OT/OTTrackMonitor/driftTimeUseVsDriftTime")) ;
   
   monapp->addPage(new MonitoringPage("PV",
				      "Track/TrackVertexMonitor/PV x position",
				      "Track/TrackVertexMonitor/PV y position",
				      "Track/TrackVertexMonitor/PV z position",
				      "Track/TrackVertexMonitor/NumTracksPerPV",
				      "Track/TrackVertexMonitor/NumLongTracksPerPV",
				      "Track/TrackVertexMonitor/NumBackTracksPerPV")) ;

   monapp->addPage(new MonitoringPage("PV chi-squares",
				      "Track/TrackVertexMonitor/NumPrimaryVertices",
				      "Track/TrackVertexMonitor/PV chisquare per dof",
				      "Track/TrackVertexMonitor/PV forward chisquare=SLASH=dof",
				      "Track/TrackVertexMonitor/PV backward chisquare=SLASH=dof",
				      "Track/TrackVertexMonitor/PV long chisquare per dof") ) ;
   
   monapp->addPage(new MonitoringPage("VELO centering",
				      "Track/TrackVertexMonitor/PV right-Right half x",
				      "Track/TrackVertexMonitor/PV right-Right half y",
				      "Track/TrackVertexMonitor/PV left-Left half x",
				      "Track/TrackVertexMonitor/PV left-Left half y"
				      )) ;

   monapp->addPage(new MonitoringPage("PV left-right",
				      "Track/TrackVertexMonitor/PV left-right delta x",
				      "Track/TrackVertexMonitor/PV left-right delta y",
				      "Track/TrackVertexMonitor/PV left-right delta z",
				      "Track/TrackVertexMonitor/PV left-right delta x versus z",
				      //"Track/TrackVertexMonitor/PV left-right delta x pull",
				      //"Track/TrackVertexMonitor/PV long chisquare per dof",
				      "Track/TrackVertexMonitor/PV left-right delta y versus z"
				      //"Track/TrackVertexMonitor/PV left-right delta y pull",
				      )) ;
   monapp->addPage(new MonitoringPage("PV left-rightVzPVz",
				      "Track/TrackVertexMonitor/PV left-right delta x versus z",
                                      "Track/TrackVertexMonitor/PV left-right delta y versus z",
				      "Track/TrackVertexMonitor/PV forward-backward delta x versus z",
				      "Track/TrackVertexMonitor/PV forward-backward delta y versus z"
				      )) ;

   monapp->addPage(new MonitoringPage("PV left-rightVzPVz",
                                      "Track/TrackVertexMonitor/PV left-right delta x versus z",
                                      "Track/TrackVertexMonitor/PV left-right delta y versus z",
                                      "Track/TrackVertexMonitor/PV forward-backward delta x versus z",
                                      "Track/TrackVertexMonitor/PV forward-backward delta y versus z"
                                      )) ;

   monapp->addPage(new MonitoringPage("PV forward-backward",
				      "Track/TrackVertexMonitor/PV forward-backward delta x",
				      "Track/TrackVertexMonitor/PV forward-backward delta y",
				      "Track/TrackVertexMonitor/PV forward-backward delta z",
				      "Track/TrackVertexMonitor/PV forward-backward delta x versus z",
				      //"Track/TrackVertexMonitor/PV forward-backward delta x pull",
				      //"Track/TrackVertexMonitor/PV long chisquare per dof",
				      "Track/TrackVertexMonitor/PV forward-backward delta y versus z"
				      //"Track/TrackVertexMonitor/PV forward-backward delta y pull",
				      )) ;

   monapp->addPage(new MonitoringPage("PV left-right (unbiased)",
				      "Track/TrackPV2HalfAlignMonitor/Left-Right PV delta x",
				      "Track/TrackPV2HalfAlignMonitor/Left-Right PV delta y",
				      "Track/TrackPV2HalfAlignMonitor/Left-Right PV delta z",
				      "Track/TrackPV2HalfAlignMonitor/PV left-right delta x versus z",
				      "Track/TrackPV2HalfAlignMonitor/PV left-right delta y versus z" )) ;

   //"Track/TrackVertexMonitor/PV chisquare per dof")) ;
   
    monapp->addPage(new MonitoringPage("IP plots",
				      "Velo/VeloIPResolutionMonitor/IPX-Vs-InversePT-LongTracks-Mean",
				      "Velo/VeloIPResolutionMonitor/IPX-Vs-Phi-LongTracks-Mean",
				      "Velo/VeloIPResolutionMonitor/IPX-Vs-Eta-LongTracks-Mean",
				      "Velo/VeloIPResolutionMonitor/IPY-Vs-InversePT-LongTracks-Mean",
				      "Velo/VeloIPResolutionMonitor/IPY-Vs-Phi-LongTracks-Mean",
				      "Velo/VeloIPResolutionMonitor/IPY-Vs-Eta-LongTracks-Mean") ) ;

    monapp->addPage(new MonitoringPage("IP in X and Y (biased)",
				      "Track/TrackVertexMonitor/track IP X",
				      "Track/TrackVertexMonitor/track IP X vs phi",
				      "Track/TrackVertexMonitor/track IP X vs eta",
				      "Track/TrackVertexMonitor/track IP Y",
				      "Track/TrackVertexMonitor/track IP Y vs phi",
				      "Track/TrackVertexMonitor/track IP Y vs eta" ) ) ;

   monapp->addPage(new MonitoringPage("IP of track with highest pT (x/y)",
				      "Track/TrackVertexMonitor/fast track IP X",
				      "Track/TrackVertexMonitor/fast track IP X vs phi",
				      "Track/TrackVertexMonitor/fast track IP X vs eta",
				      "Track/TrackVertexMonitor/fast track IP Y",
				      "Track/TrackVertexMonitor/fast track IP Y vs phi",
				      "Track/TrackVertexMonitor/fast track IP Y vs eta" ) ) ;

   monapp->addPage(new MonitoringPage("IP of track with highest pT (trans/longitudinal)",
				      "Track/TrackVertexMonitor/fast track transverse IP",
				      "Track/TrackVertexMonitor/fast track transverse IP vs phi",
				      "Track/TrackVertexMonitor/fast track transverse IP vs eta",
				      "Track/TrackVertexMonitor/fast track longitudinal IP",
				      "Track/TrackVertexMonitor/fast track longitudinal IP vs phi",
				      "Track/TrackVertexMonitor/fast track longitudinal IP vs eta" ) ) ;


   monapp->addPage(new MonitoringPage("Two-prongs",
				      "Track/TrackVertexMonitor/twoprong mass (GeV)",
				      //"Track/TrackVertexMonitor/twoprong momentum (GeV)",
				      "Track/TrackVertexMonitor/twoprong doca",
				      "Track/TrackVertexMonitor/twoprong proper lifetime (ps)",
				      "Track/TrackVertexMonitor/twoprong decaylength",
				      "Track/TrackVertexMonitor/twoprong decaylength significance",
				      "Track/TrackVertexMonitor/twoprong IP chi2 per dof") ) ;
   monapp->addPage(new MonitoringPage("Two-prong doca",
				      "Track/TrackVertexMonitor/twoprong doca",
				      "Track/TrackVertexMonitor/twoprong doca pull",
				      "Track/TrackVertexMonitor/twoprong doca vs vs eta",
				      "Track/TrackVertexMonitor/twoprong doca vs phi")) ;

   monapp->addPage(new MonitoringPage("Ks",
				      "Track/TrackV0Monitor/DownstreamDownstream/pipimass",
				      "Track/TrackV0Monitor/DownstreamDownstream/chi2prob",
				      "Track/TrackV0Monitor/DownstreamDownstream/pipimassVsPhi",
				      "Track/TrackV0Monitor/LongLong/pipimass",
				      "Track/TrackV0Monitor/LongLong/chi2prob",
				      "Track/TrackV0Monitor/DownstreamDownstream/pipimassVsPhi")) ;


   monapp->addPage(new MonitoringPage("Di-muons",
				      "Track/TrackDiMuonMonitor/mass",
				      "Track/TrackDiMuonMonitor/chi2prob",
				      "Track/TrackDiMuonMonitor/massfine",
				      "Track/TrackDiMuonMonitor/massVersusPhiMatt",
				      "Track/TrackDiMuonMonitor/multiplicity",
				      "Track/TrackDiMuonMonitor/trackchi2"
				      )) ;
   
   monapp->addPage(new MonitoringPage("More dimuons",
				      "Track/AlignParticleMonitor/mass",
				      "Track/AlignParticleMonitor/massVersusPhiMatt",
				      "Track/AlignParticleMonitor/massVersusPhi",
				      "Track/AlignParticleMonitor/massVersusMom",
				      "Track/AlignParticleMonitor/massVersusMomDif",
				      "Track/AlignParticleMonitor/massVersusMomAsym",
				      "Track/AlignParticleMonitor/massVersusTy"
				      )) ;
   
   monapp->addPage(new MonitoringPage("Velo(TT)-T match",
				      "Track/TrackFitMatchMonitor/T-TT/dx for dtx==0",
				      "Track/TrackFitMatchMonitor/T-TT/dy for dty==0",
				      "Track/TrackFitMatchMonitor/T-TT/dx pull vs tx",
				      "Track/TrackFitMatchMonitor/Velo-T/dx for dtx==0",
				      "Track/TrackFitMatchMonitor/Velo-T/dy for dty==0",
				      "Track/TrackFitMatchMonitor/Velo-T/dx pull vs tx") ) ;
   
   monapp->addPage(new MonitoringPage("Velo(TT)-T match: curvature",
				      "Track/TrackFitMatchMonitor/curvatureRatioTToLong",
				      "Track/TrackFitMatchMonitor/curvatureRatioVeloTTToLong",
				      "Track/TrackFitMatchMonitor/curvatureRatioTToLongVsQoP",
				      "Track/TrackFitMatchMonitor/curvatureRatioVeloTTToLongVsQoP") ) ;
   
   monapp->addPage(new MonitoringPage("Velo(TT)-T match: curvature (II)",
				      "Track/TrackFitMatchMonitor/curvatureRatioTToLongVsTx",
				      "Track/TrackFitMatchMonitor/curvatureRatioVeloTTToLongVsTx",
				      "Track/TrackFitMatchMonitor/curvatureRatioTToLongVsTx",
				      "Track/TrackFitMatchMonitor/curvatureRatioVeloTTToLongVsTx") ) ;
   
   monapp->addPage(new MonitoringPage("Velo-T match: position of kick",
				      "Track/TrackFitMatchMonitor/kickZ",
				      "Track/TrackFitMatchMonitor/kickZVsX",
				      "Track/TrackFitMatchMonitor/kickDeltay")) ;
  
   /*
   monapp->addPage(new MonitoringPage("M1 track match A-side",
				      "Track/TrackMuonMatchMonitor/resX_ASide_M1R1",
				      "Track/TrackMuonMatchMonitor/resX_ASide_M1R2",
				      "Track/TrackMuonMatchMonitor/resX_ASide_M1R3",
				      "Track/TrackMuonMatchMonitor/resX_ASide_M1R4",
				      "Track/TrackMuonMatchMonitor/resY_ASide_M1R1",
				      "Track/TrackMuonMatchMonitor/resY_ASide_M1R2",
				      "Track/TrackMuonMatchMonitor/resY_ASide_M1R3",
				      "Track/TrackMuonMatchMonitor/resY_ASide_M1R4") ) ;
   
   monapp->addPage(new MonitoringPage("M1 track match C-side",
				      "Track/TrackMuonMatchMonitor/resX_CSide_M1R1",
				      "Track/TrackMuonMatchMonitor/resX_CSide_M1R2",
				      "Track/TrackMuonMatchMonitor/resX_CSide_M1R3",
				      "Track/TrackMuonMatchMonitor/resX_CSide_M1R4",
				      "Track/TrackMuonMatchMonitor/resY_CSide_M1R1",
				      "Track/TrackMuonMatchMonitor/resY_CSide_M1R2",
				      "Track/TrackMuonMatchMonitor/resY_CSide_M1R3",
				      "Track/TrackMuonMatchMonitor/resY_CSide_M1R4") ) ;
      
   monapp->addPage(new MonitoringPage("Ecal-track match",
				      "Track/TrackEcalMatchMonitor/dyVsTy",
				      "Track/TrackEcalMatchMonitor/dxVsTx",
				      "Track/TrackEcalMatchMonitor/dxVsX",
				      "Track/TrackEcalMatchMonitor/E over P (inner)",
				      "Track/TrackEcalMatchMonitor/E over P (middle)",
				      "Track/TrackEcalMatchMonitor/E over P (outer)") ) ;

   monapp->addPage(new MonitoringPage("Ecal-track match A side",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (inner A-side)",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (middle A-side)",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (outer A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (inner A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (middle A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (outer A-side)") ) ;
   
   monapp->addPage(new MonitoringPage("Ecal-track match C side",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (inner C-side)",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (middle C-side)",
				      "Track/TrackEcalMatchMonitor/xEcal - xTRK (outer C-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (inner C-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (middle C-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yTRK (outer C-side)") ) ;
   
   monapp->addPage(new MonitoringPage("Velo-Ecal match",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (inner A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (middle A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (outer A-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (inner C-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (middle C-side)",
				      "Track/TrackEcalMatchMonitor/yEcal - yVELO (outer C-side)") ) ;
   */   

   monapp->addPage(new MonitoringPage("Overlap and holes on long tracks",
   				      "Track/TrackMonitor/Long/NumITOTOverlapStations",
   				      "Track/TrackMonitor/Long/NumITOverlapStations",
				      "Track/TrackMonitor/Long/NumVeloOverlapStations",
				      "Track/TrackMonitor/Long/NumVeloHoles") ) ;
   
   monapp->addPage(new MonitoringPage("Velo overlaps",
   				      "Track/TrackVeloOverlapMonitor/overlapResidualR",
				      "Track/TrackVeloOverlapMonitor/residualAR",
				      "Track/TrackVeloOverlapMonitor/residualCR",
				      "Track/TrackVeloOverlapMonitor/overlapResidualPhi",
				      "Track/TrackVeloOverlapMonitor/residualAPhi",
				      "Track/TrackVeloOverlapMonitor/residualCPhi")) ;

   monapp->addPage(new MonitoringPage("Velo overlaps Page1",
   				      "Track/TrackVeloOverlapMonitor/station0Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station1Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station2Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station0Bot/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station1Bot/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station2Bot/overlapResidualR")) ;
   
   monapp->addPage(new MonitoringPage("Velo overlaps Page2",
   				      "Track/TrackVeloOverlapMonitor/station3Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station4Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station5Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station3Bot/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station4Bot/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station5Bot/overlapResidualR")) ;

  monapp->addPage(new MonitoringPage("Velo overlaps Page3",
   				      "Track/TrackVeloOverlapMonitor/station6Top/overlapResidualR",
                                     "Track/TrackVeloOverlapMonitor/station7Top/overlapResidualR",
                "Track/TrackVeloOverlapMonitor/station8Top/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station6Bot/overlapResidualR",
   				      "Track/TrackVeloOverlapMonitor/station7Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station8Bot/overlapResidualR")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page4",
				        "Track/TrackVeloOverlapMonitor/station9Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station10Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station11Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station9Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station10Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station11Bot/overlapResidualR"));
  monapp->addPage(new MonitoringPage("Velo overlaps Page5",
				     "Track/TrackVeloOverlapMonitor/station12Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station13Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station14Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station12Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station13Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station14Bot/overlapResidualR")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page6",
				     "Track/TrackVeloOverlapMonitor/station15Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station16Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station17Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station15Bot/overlapResidualR",  
				     "Track/TrackVeloOverlapMonitor/station16Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station17Bot/overlapResidualR")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page7",
				     "Track/TrackVeloOverlapMonitor/station18Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station19Top/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station18Bot/overlapResidualR",
				     "Track/TrackVeloOverlapMonitor/station19Bot/overlapResidualR"));
  monapp->addPage(new MonitoringPage("Velo overlaps Page8",
				     "Track/TrackVeloOverlapMonitor/station0Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station1Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station2Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station0Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station1Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station2Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page9",
				     "Track/TrackVeloOverlapMonitor/station3Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station4Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station5Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station3Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station4Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station5Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page10",
				     "Track/TrackVeloOverlapMonitor/station6Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station7Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station8Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station6Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station7Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station8Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page11",
				     "Track/TrackVeloOverlapMonitor/station9Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station10Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station11Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station9Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station10Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station11Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page12",
				     "Track/TrackVeloOverlapMonitor/station12Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station13Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station14Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station12Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station13Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station14Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page13",
				     "Track/TrackVeloOverlapMonitor/station15Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station16Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station17Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station15Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station16Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station17Bot/overlapResidualPhi")) ;
  monapp->addPage(new MonitoringPage("Velo overlaps Page14",
				     "Track/TrackVeloOverlapMonitor/station18Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station19Top/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station18Bot/overlapResidualPhi",
				     "Track/TrackVeloOverlapMonitor/station19Bot/overlapResidualPhi"));
   
   monapp->addPage(new MonitoringPage("Overlap in IT1 A/C tracks",
				      "Track/TrackITOverlapMonitor/ITASideTrack/IT1TopBox/hitres",
				      "Track/TrackITOverlapMonitor/ITCSideTrack/IT1TopBox/hitres",
				      "Track/TrackITOverlapMonitor/ITASideTrack/IT1BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/ITCSideTrack/IT1BottomBox/hitres")) ;
   
   monapp->addPage(new MonitoringPage("Overlap in IT1 B/T tracks",
				      "Track/TrackITOverlapMonitor/ITBottomTrack/IT1ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITTopTrack/IT1ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITBottomTrack/IT1CSideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITTopTrack/IT1CSideBox/hitres") ) ;
   
   monapp->addPage(new MonitoringPage("Overlap in IT3 A/C tracks",
				      "Track/TrackITOverlapMonitor/ITASideTrack/IT3TopBox/hitres",
				      "Track/TrackITOverlapMonitor/ITCSideTrack/IT3TopBox/hitres",
				      "Track/TrackITOverlapMonitor/ITASideTrack/IT3BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/ITCSideTrack/IT3BottomBox/hitres") ) ;
   
   monapp->addPage(new MonitoringPage("Overlap in IT3 B/T tracks",
				      "Track/TrackITOverlapMonitor/ITBottomTrack/IT3ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITTopTrack/IT3ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITBottomTrack/IT2CSideBox/hitres",
				      "Track/TrackITOverlapMonitor/ITTopTrack/IT3CSideBox/hitres") ) ;
   

   monapp->addPage(new MonitoringPage("Overlap in IT with OT A-side tracks",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT1ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT2ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT3ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT1TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT2TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT3TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT1BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT2BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/OTASideTrack/IT3BottomBox/hitres") ) ;
   
   monapp->addPage(new MonitoringPage("Overlap in IT with OT C-side tracks",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT1ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT2ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT3ASideBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT1TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT2TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT3TopBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT1BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT2BottomBox/hitres",
				      "Track/TrackITOverlapMonitor/OTCSideTrack/IT3BottomBox/hitres") ) ;

   monapp->addPage(new MonitoringPage("IT overlap residuals",
				      "Track/ITTrackMonitor/ASide/Overlap residual",
				      "Track/ITTrackMonitor/CSide/Overlap residual",
				      "Track/ITTrackMonitor/Top/Overlap residual",
				      "Track/ITTrackMonitor/Bottom/Overlap residual") ) ;
   
   monapp->addPage(new MonitoringPage("IT overlap residuals old",
				      "Track/ITTrackMonitor/ASide/overlapResiduals",
				      "Track/ITTrackMonitor/CSide/overlapResiduals",
				      "Track/ITTrackMonitor/Top/overlapResiduals",
				      "Track/ITTrackMonitor/Bottom/overlapResiduals") ) ;
   
   monapp->addPage(new MonitoringPage("TT overlap residuals",
				      "Track/TTTrackMonitor/TTaX/Overlap residual",
				      "Track/TTTrackMonitor/TTaU/Overlap residual",
				      "Track/TTTrackMonitor/TTbV/Overlap residual",
				      "Track/TTTrackMonitor/TTbX/Overlap residual") ) ;
   
   monapp->addPage(new MonitoringPage("TT residuals in overlap region",
				      "Track/TTTrackMonitor/TTaX/Residuals in overlaps (right)",
				      "Track/TTTrackMonitor/TTaU/Residuals in overlaps (right)",
				      "Track/TTTrackMonitor/TTbV/Residuals in overlaps (right)",
				      "Track/TTTrackMonitor/TTbX/Residuals in overlaps (right)") ) ;
   

   return monapp ;
}

void drawall(const std::vector<TString>& thefiles)
{
  MonitoringApplication* monapp = configureMonApp() ;
  for(size_t i=0; i<thefiles.size(); ++i)
    monapp->addFile(thefiles[i]) ;
  monapp->draw(true) ;
}

void drawall(TString file1, TString file2="", TString file3="",TString file4="",
	     bool normalize=false)
{
  MonitoringApplication* monapp = configureMonApp() ;
  monapp->addFile(file1) ;
  if(file2 != TString("") ) monapp->addFile(file2) ;
  if(file3 != TString("") ) monapp->addFile(file3) ;
  if(file4 != TString("") ) monapp->addFile(file4) ;
  monapp->draw(true,normalize) ;
}

void drawpage(TString pagename,
	      TString file1, TString file2="", TString file3="",TString file4="",
	      bool normalize = false)
{
  MonitoringApplication* monapp = configureMonApp() ;
  monapp->addFile(file1) ;
  if(file2 != TString("") ) monapp->addFile(file2) ;
  if(file3 != TString("") ) monapp->addFile(file3) ;
  if(file4 != TString("") ) monapp->addFile(file4) ;
  monapp->drawPage(pagename,normalize) ;
}

void drawall(int lastiter, int firstiter=0,
	     bool normalize=false)
{
  MonitoringApplication* monapp = configureMonApp() ;
  char tmp[256] ;
  sprintf(tmp,"Iter%d:Iter%d/histograms.root",lastiter,lastiter) ;
  monapp->addFile(TString(tmp) ) ;
  if(lastiter != firstiter) {
    sprintf(tmp,"Iter%d:Iter%d/histograms.root",firstiter,firstiter) ;
    monapp->addFile(TString(tmp) ) ;
  }
  //drawall( files );
  monapp->draw(true,normalize) ;
}

void drawhisto(TString histo,TString file1, TString file2="", TString file3="",TString file4="",
	      bool normalize = false)
{
  MonitoringApplication* monapp = new MonitoringApplication() ;
  monapp->addFile(file1) ;
  //if(file2 != TString("") ) 
  monapp->addFile(file2) ;
  //if(file3 != TString("") ) 
  monapp->addFile(file3) ;
  //if(file4 != TString("") ) 
  monapp->addFile(file4) ;
  monapp->addPage(new MonitoringPage(histo,histo)) ;
  monapp->drawPage(histo,normalize) ;
}

