#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TPostScript.h"
#include "TFile.h"
#include "TStyle.h"
#include "TPaveStats.h"
#include "TROOT.h"
#include "TPostScript.h"
#include "TSystem.h"
#include <iostream>
#include <cmath>

TCanvas* getCanvas(const TString& name,
		   const TString& title,
		   int w=700, int h=500) 
{
  TCanvas* rc= dynamic_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject(name)) ;
  if(rc==0) rc = new TCanvas(name,title,w,h) ;
  rc->SetTitle(title) ;
  rc->Clear() ;
  return rc ;
}

void createPage(TCanvas* canvas,
		const char* title,
		int nx,
		int ny)
{
  double titlepady = 0.95 ;
  canvas->Clear() ;
  canvas->cd() ;
  char padname[1024] ;
  sprintf(padname,"%s_titlepad",canvas->GetName()) ;
  TPad* titlepad = new TPad(padname,padname,0,titlepady,1,1) ;
  titlepad->Draw() ;
  titlepad->cd() ;
  TText * titletext = new TText(0.5,0.5,title) ;
  titletext->SetNDC() ;
  titletext->SetTextSize(0.8) ;
  titletext->SetTextAlign(22) ;
  titletext->Draw() ;
  canvas->cd() ;
  gStyle->SetGridColor(16);
  
  
  //double ymargin(0),xmargin(0) ;
  double dy = 1/Double_t(ny);
  double dx = 1/Double_t(nx);
  int n(0) ;
  for (int iy=0;iy<ny;++iy) {
    double y2 = titlepady * (1 - iy*dy) ;
    double y1 = titlepady * (1 - iy*dy - dy) ;
    for (int  ix=0;ix<nx; ++ix) {
      double x1 = ix*dx;
      double x2 = x1+dx;
      ++n ;
      sprintf(padname,"%s_%d",canvas->GetName(),n);
      TPad* pad = new TPad(padname,padname,x1,y1,x2,y2);
      pad->SetNumber( n );
      pad->SetGridx() ;
      pad->SetGridy() ;
      pad->Draw();
    }
  }
  //for(int i=0; i<nx * ny; ++i) {
  //canvas->cd(i+1) ;
  //  gPad->DrawFrame(0,0,1,1) ;
  //}
}

TCanvas* createPage(const char* name,
		    const char* title,
		    int nx,
		    int ny,
		    int w=700, int h=500) 
{
  TCanvas* canvas = getCanvas(name,title,w,h) ;
  createPage(canvas,title,nx,ny) ;
  return canvas ;
}

TFile* getFile(const TString& filename)
{
  TFile* file(0) ;
  file = dynamic_cast<TFile*>(gROOT->GetListOfFiles()->FindObject(filename)) ;
  if(!file) {
    file = TFile::Open(filename) ;
  }
  return file ;
}

TObject *getObject(const TString& filename,
		   const TString& directory,
		   const TString& hisname)
{
  TFile* file = getFile(filename) ;
  TObject* h1(0) ;
  if(file && file->IsOpen()) {
    if(file->cd(directory)) {
      h1 = gDirectory->Get(hisname) ;
    } 
  } else {
    std::cout << "Cannot find file: " << filename << std::endl ;
  }
  if(h1==0)
    std::cout << "Cannot find "
	      << "object \'" << hisname << "\' "
	      << "in dir \'" << directory << "\' "
	      << "in file " << filename << "\' " << std::endl ;
  return h1 ;
}

TObject *getObject(const TString& filename,
		   const TString& name)
{
  
  int ipos = name.Last('/') ;
  TString dirname(name.Data(),ipos) ;
  TString hisname(name.Data()+ipos+1) ;
  return getObject(filename,dirname,hisname) ;
}

TH1* getH1(const TString& filename,
	   const TString& hisname)
{
  TH1* h1 = dynamic_cast<TH1*>(getObject(filename,hisname)) ;
  if(h1) {
    h1->UseCurrentStyle() ;
  }
  return h1 ;
}

TH2* getH2(const TString& filename,
	   const TString& hisname)
{
  TH2* h2 = dynamic_cast<TH2*>(getObject(filename,hisname)) ;
  if(h2) {
    h2->UseCurrentStyle() ;
  }
  return h2 ;
}

bool is1D( TH1* h1 ) {
  return h1 && h1->GetNbinsY()==1 && dynamic_cast<TProfile*>(h1)==0 ;
}

bool isProfile1D( TH1* h1 ) {
  return h1 && h1->GetNbinsY()==1 && dynamic_cast<TProfile*>(h1)!=0 ;
}

double max1D( const TH1& h1 ) {
  int firstbin = h1.GetXaxis()->GetFirst() ;
  int lastbin  = h1.GetXaxis()->GetLast() ;
  double rc = -1e9 ;
  const TProfile* pr = dynamic_cast<const TProfile*>(&h1) ;
  for(int ibin=firstbin; ibin<=lastbin; ++ibin)
    if(!pr || pr->GetBinEntries(ibin)>0) 
      rc = std::max(rc,h1.GetBinContent(ibin)) ;
  return rc ;
}

double min1D( const TH1& h1 ) {
  int firstbin = h1.GetXaxis()->GetFirst() ;
  int lastbin  = h1.GetXaxis()->GetLast() ;
  double rc = 1e9 ;
  const TProfile* pr = dynamic_cast<const TProfile*>(&h1) ;
  for(int ibin=firstbin; ibin<=lastbin; ++ibin)
     if(!pr || pr->GetBinEntries(ibin)>0) 
       rc = std::min(rc,h1.GetBinContent(ibin)) ;
  return rc ;
}

TH1* drawH1(TString filename,
	    TString hisname,
	    const char* drawopt="",
	    int color=1,
	    bool normalize = false)
{
  TH1* h1 = getH1(filename,hisname) ;
  if(h1) {
    h1->SetLineColor(color) ;
    h1->SetLineWidth(2) ;
    h1->SetMarkerColor(color) ;
    if (color==2)
      h1->SetLineStyle(2) ;
    else if (color==9)
      h1->SetLineStyle(3) ;
    else if (color==8)
      h1->SetLineStyle(4) ;

    if(normalize && is1D(h1) ) {
      double integral = h1->Integral() ;
      if(integral >0 && fabs(integral-1)>1e-6) {
	if( h1->GetSumw2()==0 ) h1->Sumw2() ;
	h1->Scale(1/integral) ;
      }
    }
    h1 = h1->DrawCopy(drawopt) ;
    //h1->Draw(drawopt) ;
  }
  return h1 ;
}


TH1* drawH2(TString filename,
	    TString hisname,
	    const char* drawopt="",
	    int color=1)
{
  TH1* h1 = getH1(filename,hisname) ;
  TH2* h2 = dynamic_cast<TH2*>(h1) ;
  TH1* rc = 0 ;
  if(h2) {
    TH1* tmp = h2 ;
    TString stringopt(drawopt) ;
    TH1* owned(0) ;
    char tmpname[1024] ;
    sprintf(tmpname,"%s-%s_tmp",filename.Data(),hisname.Data()) ;
    if( stringopt.Contains("ProfX") ) {
      owned = tmp = h2->ProfileX(tmpname) ;
      stringopt.ReplaceAll("ProfX","") ;
    } else if ( stringopt.Contains("ProjX") ) {
      owned = tmp = h2->ProjectionX(tmpname) ;
      stringopt.ReplaceAll("ProjX","") ;
    } else if( stringopt.Contains("ProjY") ) {
      owned = tmp = h2->ProjectionY(tmpname) ;
      stringopt.ReplaceAll("ProjY","") ;
    }
    tmp->SetTitle(h2->GetTitle()) ;
    tmp->UseCurrentStyle() ;
    tmp->SetLineColor(color) ;
    tmp->SetLineWidth(2) ;
    tmp->SetMarkerColor(color) ;
    if (color==2)
      tmp->SetLineStyle(2) ;
    else if (color==9)
      tmp->SetLineStyle(3) ;
    else if (color==8)
      tmp->SetLineStyle(4) ;
    rc = tmp->DrawCopy(stringopt) ;
    delete owned ;
  }
  return rc ;
}

int colorFromIndex(int index)
{
  int colors[] = {1,2,9,8,5,6,7} ;
  return colors[index] ;
}


TH1* drawH1(const std::vector<TString>& files,
            const TString& hisname,
            TString drawopt = "",
            bool normalize = false,
            Double_t new_miny = 0,
            Double_t new_maxy = 0
)
{
  TH1* rc = drawH1(files[0],hisname,drawopt,colorFromIndex(0),false) ;
  if(rc ) {
    double norm = rc->Integral() ;
    
    if(  drawopt=="" ) {
      drawopt = is1D(rc) ? "hist" : "err" ;
      rc->SetDrawOption(drawopt) ;
      if( isProfile1D(rc) ) rc->SetStats(0) ;  
    }
    
    double minval = min1D(*rc) ;
    double maxval = max1D(*rc) ;
    for( unsigned int i=1; i<files.size(); ++i) 
      if(files[i].Length()>0) {
        TH1* h1 = drawH1(files[i],hisname,drawopt + " sames",colorFromIndex(i),false) ;
        if(h1) {
          if( isProfile1D(rc) ) rc->SetStats(0) ;
          else {
            TPaveStats *st = (TPaveStats*)h1->FindObject("stats") ;
            if(st) st->SetTextColor(colorFromIndex(i)) ;
          }
          if( is1D(h1) && normalize ) {
            if( h1->GetSumw2()==0 ) h1->Sumw2() ;
            h1->Scale(norm/h1->Integral()) ;
          }
          maxval = std::max(maxval,max1D(*h1)) ;
          minval = std::min(minval,min1D(*h1)) ;
        }
      }
    
    if ( is1D(rc) ) {
      if( ! gPad->GetLogy() ) {
        if(maxval >  max1D(*rc) ) rc->SetMaximum(1.1*maxval) ;
        rc->SetMinimum(0) ;
      } else {
        rc->SetMaximum(2*maxval) ;
        rc->SetMinimum(minval>0 ? 0.5*minval : 0.5) ;
      }
    } else if( isProfile1D(rc) ) {
      double miny,maxy ;
      if( minval<0 && maxval>0) {
        // symmetrize 
        maxy = 1.1 * std::max(-minval,maxval) ;
        miny = -maxy ;
      } else {
        maxy = maxval + 0.1*(maxval-minval) ;
        miny = minval - 0.1*(maxval-minval) ; 
        if(minval>=0 && miny<0) miny=0 ;
      }
      //if( maxy > rc->GetMaximum() ) rc->SetMaximum(maxy) ;
      //if( miny < rc->GetMinimum() ) rc->SetMinimum(miny) ;
      rc->SetMaximum( maxy ) ;
      rc->SetMinimum( miny ) ;
    }
  } else {
    gPad->Clear() ;
  }
  if ((is1D(rc) || isProfile1D(rc)) && new_miny != new_maxy)
  {
    rc->SetMaximum( new_maxy ) ;
    rc->SetMinimum( new_miny ) ;
  }
  return rc ;
}

TH1* drawH2(std::vector<TString> files,
	    TString hisname,
	    TString drawopt)
{
  TH1* rc = drawH2(files[0],hisname,drawopt) ;
  for( unsigned int i=1; i<files.size(); ++i)
    drawH2(files[i],hisname,drawopt + " sames",colorFromIndex(i)) ;
  return rc ;
}

/*******************************************************************************/

class MonitoringPage
{
public:
  MonitoringPage(const char* atitle,
		 const char* h1=0,
		 const char* h2=0,
		 const char* h3=0,
		 const char* h4=0,
		 const char* h5=0,
		 const char* h6=0,
		 const char* h7=0,
		 const char* h8=0,
		 const char* h9=0)
    : m_title(atitle)
  {
    // should use a 'variadic' function here
    if(h1) m_h.push_back(h1) ;
    if(h2) m_h.push_back(h2) ;
    if(h3) m_h.push_back(h3) ;
    if(h4) m_h.push_back(h4) ;
    if(h5) m_h.push_back(h5) ;
    if(h6) m_h.push_back(h6) ;
    if(h7) m_h.push_back(h7) ;
    if(h8) m_h.push_back(h8) ;
    if(h9) m_h.push_back(h9) ;
  }

  // void
//   draw(const std::vector<TString>& filenames, bool print ) {
//     TCanvas* c1 = getCanvas(m_title,m_title) ;
//     draw( filenames, c1 ) ;
    
//     if(print) c1->Print(m_title + ".eps") ;
//   }

  void
  draw(const std::vector<TString>& filenames, TCanvas* canvas, bool normalize ) {
    size_t ny = 2 ;
    size_t nx = std::max(int(m_h.size()/ny),2) ;
    if( nx*ny < m_h.size()) ++nx ;
    if(m_h.size()==1) ny=nx=1 ;
    TCanvas* c1 = canvas ? canvas : getCanvas(m_title,m_title) ;
    createPage(c1,m_title,nx,ny) ;
    for( size_t i=0; i< m_h.size() ; ++i) {
      c1->cd(i+1) ;
      drawH1(filenames,m_h[i],"",normalize) ;
    }
   
  }

  TString GetName() const { return m_title ; }

private:
  TString m_title ;
  std::vector<TString> m_h ;

};


/******************************************************************************/

class MonitoringApplication
{
public:
  MonitoringApplication() {}
  void addPage( MonitoringPage* page) { m_pages.push_back(page) ; }
  void addFile( const TString& filenametag) { 
    int ipos = filenametag.First(":") ;
    TString filename = filenametag ;
    TString tag ;
    if(ipos > 0 && ipos < filenametag.Sizeof()) {
      tag = TString(filenametag.Data(),ipos) ;
      filename = TString(filenametag.Data()+ipos+1) ;
    }
    m_filenames.push_back(filename) ;
    m_tags.push_back(tag) ;
  }

  void drawPage(TString pagename, bool normalize=false) {
    bool found = false ;
    for( size_t ipage=0; ipage<m_pages.size() && !found; ++ipage) 
      if( m_pages[ipage]->GetName() == pagename ) {
	found = true ;
	m_pages[ipage]->draw(m_filenames,0,normalize) ;
	gPad->GetCanvas()->cd(1) ;
	TText text ;
	text.SetNDC() ;
	for(size_t i=0; i<m_tags.size(); ++i) {
	  if(m_tags[i] != TString()) {
	    text.SetTextColor( colorFromIndex(i) ) ;
	    text.DrawText(0.7,0.5-0.1*i,m_tags[i]) ;
	  }
	}
	gPad->GetCanvas()->Print("c1.eps") ;
      }
    if(!found) {
      std::cout << "Cannot find page calles \'" << pagename << "\'. Please choose from:" << std::endl ;
      for( size_t ipage=0; ipage<m_pages.size(); ++ipage) 
	std::cout << m_pages[ipage]->GetName() << std::endl ;
    }
  }
  
  void draw(bool print=false, bool normalize=false) {
    TPostScript* ps(0) ;
    TCanvas* c1(0) ;
    if(print) {
      ps = new TPostScript("plots.ps",112) ;
      ps->Range(26,18);  
      c1 = new TCanvas("LHCbMonitoring","") ;
    }
    
    for( size_t ipage=0; ipage<m_pages.size(); ++ipage) {
      m_pages[ipage]->draw(m_filenames,c1,normalize) ;
      gPad->GetCanvas()->cd(1) ;
      TText text ;
      text.SetNDC() ;
      for(size_t i=0; i<m_tags.size(); ++i) {
	if(m_tags[i] != TString()) {
	  text.SetTextColor( colorFromIndex(i) ) ;
	  text.DrawText(0.8,0.5-0.1*i,m_tags[i]) ;
	}
      }
      if(c1) c1->Update() ;
    }
    ps->Close() ;
    gSystem->Exec("ps2pdf plots.ps") ;
  }
  
private:
  std::vector<MonitoringPage*> m_pages ;
  std::vector<TString> m_filenames ;
  std::vector<TString> m_tags ;

} ;

static MonitoringApplication* gMonitoringApplication = new MonitoringApplication() ;

