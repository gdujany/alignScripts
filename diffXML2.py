#!/usr/bin/env python

dict_max_diff = {'Tx' : 0.0015, 'Ty' : 0.0015, 'Tz' : 0.005, 'Rx' : 0.000004, 'Ry' : 0.000004, 'Rz' : 0.00003}


##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ='read 2 xml files and give the different ...')
    parser.add_argument('file1',help='First file')
    parser.add_argument('file2',help='Second file')
    parser.add_argument('-l','--limits',help='''limits eg "Tx:0.0002 Ty:0.0002 Tz:0.0002, the one not given will keep the defaults''', nargs='+', type=str)
    parser.add_argument('--regex',help='compare only alignables which mach regex', nargs='+', default=['.*'])
    args = parser.parse_args()
##########################

import xml.etree.ElementTree
import os, re

def readXML(inFile):
    '''
    Read XML file and return dictionary with {name: align_params}
    where align_params is a list [Tx, Ty, Tz, Rx, Ry, Rz]
    '''
    text = open(inFile).read()
    if '<DDDB>' not in text:
        text = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"><DDDB>\n'+text+'\n</DDDB>'
    root = xml.etree.ElementTree.fromstring(text)
    alignParams = {}
    for alignable in root:
        name = alignable.attrib['name']
        for vec in alignable:
            if vec.attrib['name'] == 'dPosXYZ':
                [Tx, Ty, Tz] = [float(i) for i in vec.text.split()]
            elif vec.attrib['name'] == 'dRotXYZ':
                [Rx, Ry, Rz] = [float(i) for i in vec.text.split()] 

        alignParams[name] = [Tx, Ty, Tz, Rx, Ry, Rz]
    return alignParams


def getDictDifference(file1, file2):
    """
    Read two XML files and return dictionary with {name: diff}
    where diff is a list with the differences [dTx, dTy,  dTz, dRx, dRy, dRz]
    """
    align1 = readXML(file1)
    align2 = readXML(file2)
    diff = {}
    for key in set(align1.keys() + align2.keys()):
        try:
            diff[key] = [i-j for i,j in zip(align1[key], align2[key])]
        except KeyError:
            pass
    return diff


def bigChanges(diff, list_max_diff, regexes = ['.*']):
    '''
    Return the dictionary of alignables which deviated more than they should have
    {alignable_dof : param_diff}
    '''
    bigs = {}
    dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
    for name, params in diff.items():
        for regex in regexes:
            if re.match(regex, name):
                for param, par_max, dof in zip(params, list_max_diff, dofs):
                    if abs(param) > par_max:
                        bigs['{name}_{dof}'.format(**locals())] = param
    return bigs


def get2Files(directory):
    return sorted(os.listdir(directory))[:2]


if __name__ == '__main__':
    
    dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']

    if args.limits:
        try:
            new_limits = [(i.split(':')[0], float(i.split(':')[1])) for i in args.limits]
            for dof in new_limits:
                if dof[0] not in dofs:
                    raise IOError('{0}, not good dof, only accepted are {1}'.format(dof[0], dofs))
        except IndexError:
            raise IOError('Input limits not in the correct format eg Tx:0.0002')

        dict_max_diff.update(new_limits)

        
    list_max_diff = [dict_max_diff[key] for key in dofs]
    print 'limits:', list_max_diff
    diff = getDictDifference(args.file1, args.file2)
    bigs_dict = bigChanges(diff, list_max_diff, args.regex)
    bigs = sorted(bigs_dict.keys())
    print len(bigs), 'alignables which exceed limits:', bigs
    print '{0:<15} {1:<15} {2:<15}'.format('Alignable_dof', 'Value', 'difference wrt limit')
    for big in bigs:
        print '{0:<15} {1:<15.3} {2:<15.3}'.format(big, bigs_dict[big], abs(bigs_dict[big])-dict_max_diff[big[-2:]])
    

