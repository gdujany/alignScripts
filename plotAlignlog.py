#!/usr/bin/env python

import os, sys
# from AlignmentOutputParser.AlPlotter import *
# from AlignmentOutputParser.AlignOutput import *
from alignlogPlotter.AlPlotter import *
from alignlogPlotter.AlignOutput import *
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
###########################
# Load log-files

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make plots from alignlog")
    parser.add_argument('files',help='''files to process,
es label1:/path/to/file1/alignlog.txt  label2:/path/to/file2/alignlog.txt
 It should work also with label1:/path/to/file1/  label2:/path/to/file2/
''', nargs='*', type=str)
    parser.add_argument('-o','--outFile',help='output file name do not put the extension because it will create both file and forder',default='plotsIterations')
    parser.add_argument('-d','--plotsDir',help='create folder with a png for each plot',nargs='?',const='plotsIterations')
args = parser.parse_args()

##########################

def findPaths(file_name, startingPath):
    if fnmatch.fnmatch(os.path.split(startingPath)[1], file_name):
        return startingPath
    for root, dirs, files in os.walk(startingPath):
        for file in files:
            if fnmatch.fnmatch(file, file_name):
                return root+'/'+file

list_files = [[i.split(':')[0], os.path.normpath(os.path.expandvars(os.path.expanduser(i.split(':')[1])))] for i in args.files]

list_files = [[i[0], findPaths('alignlog.txt', i[1])] for i in list_files]


alIters = []
numIters = []
second_labels = []
for label, fileName in list_files:
    aout = AlignOutput(fileName)
    aout.Parse()
    alIters += aout.AlignIterations
    numIters.append(len(aout.AlignIterations))
    second_labels.append(label)

separations = [sum(numIters[:i], -0.5) for i in range(len(numIters)+1)]


###########################
# Check img directory existence
if args.plotsDir:
    imgPath = '{0}/'.format(args.plotsDir)
    if not os.path.exists(imgPath): os.makedirs(imgPath)

###########################
# Create Pdf file
pdf = PdfPages('{0}.pdf'.format(args.outFile))

###########################
# Load info into plots creator
apl = AlPlotter(alIters)

apl.xAxis['ticknames'] = sum([range(1,i+1) for i in numIters], [])

bigs = [i for i in alIters if isBigDiff(i)]

def prettifyPlot():
    for cont in range(len(separations))[:-1]:
        x_coord = (separations[cont]+separations[cont+1])/2.
        offset = 10 if cont%2==0 else 20
        plt.annotate(second_labels[cont], (x_coord,0), (x_coord, offset), xycoords=("data", "axes fraction"), va='center', ha='center', textcoords=('data', 'offset points'))# textcoords='offset points')
    for sep in separations[1:-1]:
        plt.axvline(sep, ls='--', color='k')

# Plot Chi2
for expression in ['NormalisedChi2Change']:
    apl.PlotExpression(expression, ylog=True)
    prettifyPlot()
    plt.axhline(2, ls='--', color='k')
    if args.plotsDir:
        apl.Plots[-1].savefig(imgPath+expression.replace('/',''))
    pdf.savefig() 
    
for expression in ['MaxModeChi2']:
    apl.PlotExpression(expression, ylog=True)
    prettifyPlot()
    plt.axhline(25, ls='--', color='k')
    if args.plotsDir:
        apl.Plots[-1].savefig(imgPath+expression.replace('/',''))
    pdf.savefig()


for expression in ['Chi2/NDOFs', 'Nevs', 'tracks', 'vertices']:
    apl.PlotExpression(expression)
    prettifyPlot()
    if args.plotsDir:
        apl.Plots[-1].savefig(imgPath+expression.replace('/',''))
    pdf.savefig()

    
Correction = {'delta': 'correction to initial'}
for cname, cdesc in Correction.iteritems():

    # VELO Halves
    try:
        dofs_limits = {'Tx' : 0.002, 'Ty' : 0.002, 'Tz' : 0.004, 'Rx' : 0.0000035, 'Ry' : 0.0000035, 'Rz' : 0.000015}
        print 'VELO halves', cname
        alignables = ['Velo/VeloLeft', 'Velo/VeloRight']
        dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
        for dof in dofs:
            mdofs = ['{0}.{1}'.format(i,dof) for i in alignables]
            apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='Velo halves '+dof+' ('+cname+')', yrange= None)
            prettifyPlot()
            try:
                for dof_limit in dofs_limits[dof]:
                    plt.axhline(dof_limit, ls='--', color='k')
                    plt.axhline(-1*dof_limit, ls='--', color='k')
            except TypeError:
                plt.axhline(dofs_limits[dof], ls='--', color='k')
                plt.axhline(-1*dofs_limits[dof], ls='--', color='k')
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'2halves'+'_'+dof+'_'+cname)
            pdf.savefig()
            plt.close()
    except (KeyError, AttributeError):
        pass


    # VELO Modules
    try:
        dofs_limits = {'Tx' : 0.002, 'Ty' : 0.002, 'Rz' : 0.000100}
        print 'VELO Modules', cname
        modules_A1 = ['Velo/VeloLeft/Module{0:0>2}'.format(i) for i in range(0, 20, 2)]
        modules_A2 = ['Velo/VeloLeft/Module{0:0>2}'.format(i) for i in range(20, 41, 2)]
        modules_C1 = ['Velo/VeloRight/Module{0:0>2}'.format(i) for i in range(1, 21, 2)]
        modules_C2 = ['Velo/VeloRight/Module{0:0>2}'.format(i) for i in range(21, 42, 2)]
        for alignables, modSet in zip([modules_A1, modules_A2, modules_C1, modules_C2], ['A1', 'A2', 'C1', 'C2']):
            dofs = ['Tx', 'Ty', 'Rz']
            for dof in dofs:
                # yranges = None#{'1': {'min':-0.01,'max':0.01}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
                mdofs = ['{0}.{1}'.format(i,dof) for i in alignables]
                apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='Velo Modules '+modSet+' '+dof+' ('+cname+')', yrange= None)
                prettifyPlot()
                try:
                    for dof_limit in dofs_limits[dof]:
                        plt.axhline(dof_limit, ls='--', color='k')
                        plt.axhline(-1*dof_limit, ls='--', color='k')
                except TypeError:
                    plt.axhline(dofs_limits[dof], ls='--', color='k')
                    plt.axhline(-1*dofs_limits[dof], ls='--', color='k')
                    if args.plotsDir:
                        apl.Plots[-1].savefig(imgPath+'Modules_'+modSet+'_'+dof+'_'+cname)
                pdf.savefig()
            plt.close()
    except KeyError:
        pass


    # TT Layers
    try:
        print 'TT Layers', cname
        yranges = None#{'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
        dof = 'Tz'
        mdofs = []    
        for lay in ['aX','aU','bV','bX']:
            mdofs += ['TT/TT'+str(lay[0])+'/TT'+str(lay)+'Layer.'+dof]
        apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='TTLayers '+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
        if args.plotsDir:
            apl.Plots[-1].savefig(imgPath+'TTLayers_'+dof+'_'+cname)
        pdf.savefig()
        plt.close()
    except KeyError:
        pass


    # TT Modules
    try:
        print 'TT modules', cname
        yranges = None#{'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
        dof = 'Tx'
        TTModulesStruct = { 'a': {1: xrange(1,7), 2:[1,'2B','2T',3], 3:xrange(1,7)},
                            'b': {1: xrange(1,7), 2:[1,2,'3B','3T',4,5], 3:xrange(1,7)}}
        for lay in ['aX','aU','bV','bX']:
            for sector in xrange(1,4):
                mdofs = []
                for module in TTModulesStruct[lay[0]][sector]:
                    mdofs += ['TT'+lay+'LayerR'+str(sector)+'Module'+str(module)+'.'+dof]
                apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='TT'+lay+' R'+str(sector)+' Modules '+dof+' ('+cname+')', yrange= yranges[str(sector)] if yranges !=None else yranges)
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'TTLayer_'+lay+'_R'+str(sector)+'_'+dof+'_'+cname)
                pdf.savefig()
                plt.close()
    except KeyError:
        pass

    
    # IT Boxes
    try:
        print 'IT boxes', cname
        dofs = ['Tx','Tz','Rz']
        for dof in dofs:
            yranges = {'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
            for stat in xrange(1,4):
                mdofs = []
                for box in ['Top','Bottom','ASide','CSide']:
                    mdofs += ['IT/Station'+str(stat)+'/'+box+'Box.'+dof]
                apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='IT'+str(stat)+' Boxes '+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'Station'+str(stat)+'_'+dof+'_'+cname)
                pdf.savefig()
                plt.close()
    except KeyError:
        pass


    # OT Frames
    try:
        print 'OT C-frames', cname            
        dofs = ['Tx','Rz']
        for dof in dofs:
            yranges = None# {'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
            for stat in xrange(1,4):
                mdofs = []
                for box in ['%s%sSide' % (a,b) for a in ['X1U','VX2'] for b in ['A','C']]:
                    mdofs += ['OT/T'+str(stat)+box+'.'+dof]
                apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='OT'+str(stat)+' C-frames '+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'T'+str(stat)+'_Cframes_'+dof+'_'+cname)
                pdf.savefig()    
                plt.close()
    except KeyError:
        pass


    # OT Layers
    try:
        print 'OT Layers', cname
        dofs = ['Tz']
        for dof in dofs:
            yranges = None# {'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
            for stat in xrange(1,4):
                mdofs = []
                for box in ['X1U','VX2']:
                    mdofs += ['OT/T'+str(stat)+box+'.'+dof]
                apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='OT'+str(stat)+' layers '+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'T'+str(stat)+'_layers_'+dof+'_'+cname)
                pdf.savefig()  
                plt.close()
    except KeyError:
        pass

    # OT Modules
    try:
        print 'OT modules'
        # Loop on quadrants and stations
        dofs = ['Tx','Rz']
        for dof in dofs:
            yranges = None# {'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
            for stat in xrange(1,4):
                for lay in ['X1','U','V','X2']:
                    for quad in ['Q(0|2)','Q(1|3)']:
                        mdofs = ['OT/T'+str(stat)+lay+quad+'M'+str(module)+'.'+dof for module in xrange(1,10)]
                        apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='OT'+str(stat)+lay+quad+' modules '+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
                        if args.plotsDir:
                            apl.Plots[-1].savefig(imgPath+'T'+str(stat)+lay+quad+'_modules_'+dof+'_'+cname)
                        pdf.savefig()  
                        plt.close()
    except KeyError:
        pass

    # Muons
    try:
        print 'Muon ', cname
        # Plot IT boxes
        dofs = ['Tx','Ty']
        for dof in dofs:
            yranges = {'1': {'min':-15,'max':20.}, '2': {'min':-5,'max':+6}, '3': {'min':-1.5,'max':2.5} } if dof == 'Tz' else None
            for stat in xrange(1,6):
                mdofs = []
                for box in ['ASide','CSide']:
                    mdofs += ['Muon/M'+str(stat)+'/M'+str(stat)+box+'.'+dof]
                    dum='Muon'+str(stat)+dof+' ('+cname+')'
                    mycondition='Muon/M'+str(stat)+'/M'+str(stat)+box
                    #print mdofs, "corr=",cdesc, "title=",dum, "yrange=", yranges[str(stat)], yranges
                    apl.PlotAlignablesDofs( mdofs, corr=cdesc, title='Muon'+str(stat)+dof+' ('+cname+')', yrange= yranges[str(stat)] if yranges !=None else yranges)
                    print mycondition,dof,cname
                    #apl.PlotAlignableDof(mycondition,dof,par=cname)
                if args.plotsDir:
                    apl.Plots[-1].savefig(imgPath+'Station'+str(stat)+'_'+dof+'_'+cname)
                pdf.savefig()
                plt.close()
    except KeyError:
        pass

   
# Close the pdf
pdf.close()



