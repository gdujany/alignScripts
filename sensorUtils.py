from math import sqrt, sin, cos
from ROOT import TGraphErrors, TMultiGraph, TLegend, TCanvas, TH1D
import xml.etree.ElementTree
from array import array
import ROOT, sys, os, fnmatch
sys.path.append('/afs/cern.ch/user/g/gdujany/pyUtils')
from MultiPlot import MultiPlot

this_dir = os.path.dirname(os.path.realpath(__file__))

#funz. per passare da indice di array a numero di sensore
def indMiei(numsens):    
    histind=numsens  
    if (numsens>=42):
        histind-=22  
    return histind

def indVeri(nsens,nmod):
    numsens=nmod
    if(((nmod%4<2) and (nsens==1)) or ((nmod%4>1) and (nsens==0))):
        numsens+=64
    return numsens

def getModSens(histind):
    isR = histind<42
    nmod = histind if isR else histind-64
    nsens = 0 if nmod%4<2 else 1
    if not isR: nsens = int(not nsens)
    return nmod, nsens

zPos = range(-175,291,15)+[435, 450]+sum([[i-15, i] for i in range(600,751,50)],[])

colori=[ROOT.kBlue, ROOT.kRed, ROOT.kGreen+2, ROOT.kMagenta+1, ROOT.kOrange-3, ROOT.kYellow, ROOT.kCyan, ROOT.kBlack]
markersA=[20,21,22,23,29,33,34,8]
markersC=[24,25,26,32,30,27,28,4]

class SensAbs(object):
    '''
    Abstract class for storing information about a sensor es misalignmets, number of hits collected
    each sensor has a sensor number, and a dictionary {variable : value}
    '''
    def __init__(self, numsens, vars_dict):
        self.__dict__ = vars_dict
        self.numsens=numsens
        self.histind=numsens
        if (numsens>=42):
            self.histind-=22
    

    def __add__(self, other):
        vars_dict= self._somma(self.__dict__, other.__dict__)
        ret = SensAbs(self.numsens, vars_dict)
        ret.__class__ = self.__class__ # like this I cast the base class to the derived class
        return ret

    def _somma(self, my_vars, other_vars):
        '''
        Here implement what to do when summing sensors,
        to be overloded in daughters classes
        '''
        raise NotImplementedError('_somma should be implemented in daughter class')
        
    def __sub__(self, other):
        vars_dict = self._differenza(self.__dict__, other.__dict__)
        ret = SensAbs(self.numsens, vars_dict)
        ret.__class__ = self.__class__
        return ret

    def _differenza(self, my_vars, other_vars):
        '''
        Here implement what to do when subtracting sensors,
        to be overloded in daughters classes
        '''
        raise NotImplementedError('_differenza should be implemented in daughter class')

    def __abs__(self):
        vars_dict= self._absolute(self.__dict__)
        ret = SensAbs(self.numsens, vars_dict)
        ret.__class__ = self.__class__
        return ret

    def _absolute(self, my_vars):
        '''
        Here implement what to do when doing abs sensors,
        to be overloded in daughters classes
        '''
        raise NotImplementedError('_absolute should be implemented in daughter class')

    def __div__(self, number):
        vars_dict = self._divide(self.__dict__, number)
        ret = SensAbs(self.numsens, vars_dict)
        ret.__class__ = self.__class__
        return ret

    def _divide(self, my_vars, number):
        '''
        Here implement what to do when subtracting sensors,
        to be overloded in daughters classes
        '''
        raise NotImplementedError('_divide should be implemented in daughter class')

        
class SensMis(SensAbs):    
    '''
    Class which defines misalignments of a sensor and implement difference, absolute value and string representation
    '''
    def __init__(self,numsens,dx=0,dy=0,dz=0,dalpha=0,dbeta=0,dgamma=0,edx=0,edy=0,edz=0,edalpha=0,edbeta=0,edgamma=0):
        vars_dict = dict(
            dx=dx*1000, #*1000 per passare da mm a um e da rad a mrad
            dy=dy*1000,
            dz=dz*1000,
            dalpha=dalpha*1000,
            dbeta=dbeta*1000,
            dgamma=dgamma*1000,
            edx=edx*1000,
            edy=edy*1000,
            edz=edz*1000,
            edalpha=edalpha*1000,
            edbeta=edbeta*1000,
            edgamma=edgamma*1000,)
        SensAbs.__init__(self, numsens, vars_dict)

    @property
    def Tx(self): return self.dx
    @Tx.setter
    def Tx(self, value): self.dx = value
    @property
    def Ty(self): return self.dy
    @Ty.setter
    def Ty(self, value): self.dy = value
    @property
    def Tz(self): return self.dz
    @Tz.setter
    def Tz(self, value): self.dz = value
    @property
    def Rx(self): return self.dalpha
    @Rx.setter
    def Rx(self, value): self.dalpha = value
    @property
    def Ry(self): return self.dbeta
    @Ry.setter
    def Ry(self, value): self.dbeta = value
    @property
    def Rz(self): return self.dgamma
    @Rz.setter
    def Rz(self, value): self.dgamma = value

    @property
    def eTx(self): return self.edx
    @eTx.setter
    def eTx(self, value): self.edx = value
    @property
    def eTy(self): return self.edy
    @eTy.setter
    def eTy(self, value): self.edy = value
    @property
    def eTz(self): return self.edz
    @eTz.setter
    def eTz(self, value): self.edz = value
    @property
    def eRx(self): return self.edalpha
    @eRx.setter
    def eRx(self, value): self.edalpha = value
    @property
    def eRy(self): return self.edbeta
    @eRy.setter
    def eRy(self, value): self.edbeta = value
    @property
    def eRz(self): return self.edgamma
    @eRz.setter
    def eRz(self, value): self.edgamma = value 

    def getListMis(self):
        return [self.dx, self.dy, self.dz, self.dalpha,self.dbeta,self.dgamma]

    def setListMis(self,listMis):
        self.dx = listMis[0]
        self.dy = listMis[1]
        self.dz = listMis[2]
        self.dalpha = listMis[3]
        self.dbeta = listMis[4]
        self.dgamma = listMis[5]
    
    def _somma(self, my_vars, other_vars):
        ret_dict = {}
        for var in ('dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'):
            ret_dict[var] = my_vars[var] + other_vars[var]
        for var in ('edx', 'edy', 'edz', 'edalpha', 'edbeta', 'edgamma'):
            ret_dict[var] = sqrt(my_vars[var]**2 + other_vars[var]**2)
        return ret_dict


    def _differenza(self, my_vars, other_vars):
        ret_dict = {}
        for var in ('dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma'):
            ret_dict[var] = my_vars[var] - other_vars[var]
        for var in ('edx', 'edy', 'edz', 'edalpha', 'edbeta', 'edgamma'):
            ret_dict[var] = sqrt(my_vars[var]**2 + other_vars[var]**2)
        return ret_dict

    def _absolute(self, my_vars):
        ret_dict = {}
        for var in my_vars:
            ret_dict[var] = abs(my_vars[var])
        return ret_dict

    def _divide(self, my_vars, number):
        ret_dict = {}
        for var in ('dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma', 'edx', 'edy', 'edz', 'edalpha', 'edbeta', 'edgamma'):
            ret_dict[var] = my_vars[var] / number
        return ret_dict
        

    def __str__(self):
        #return str(self.numsens)+"\t"+str(self.dx)+"\t"+str(self.dy)+"\t"+str(self.dz)+"\t"+str(self.dalpha)+"\t"+str(self.dbeta)+"\t"+str(self.dgamma)+"\t"+str(self.edx)+"\t"+str(self.edy)+"\t"+str(self.edz)+"\t"+str(self.edalpha)+"\t"+str(self.edbeta)+"\t"+str(self.edgamma)
        return '{0:<4} {1:>13.6} {2:>13.6} {3:>13.6} {4:>13.6} {5:>13.6} {6:>13.6} {7:>13.6} {8:>13.6} {9:>13.6} {10:>13.6} {11:>13.6} {12:>13.6}'.format(self.numsens, float(self.dx), float(self.dy), float(self.dz), float(self.dalpha), float(self.dbeta), float(self.dgamma), float(self.edx), float(self.edy), float(self.edz), float(self.edalpha), float(self.edbeta), float(self.edgamma))


class _rangeType:
    '''
    Container class only to store information
    '''
    def __init__(self, sensMax=(0,42), sensNums=range(42), zPositions=zPos):
        self.sensMax = sensMax
        self.sensNums = sensNums
        self.zPositions = zPositions

_range_r = _rangeType()
_range_phi = _rangeType(sensMax=(42,84), sensNums=range(64,106)*2, zPositions=zPos*2)
_range_both = _rangeType(sensMax=(0,84), sensNums=range(42)+range(64,106), zPositions=zPos + [i+1425 for i in zPos])


class SensAbsCollection:
    
    def __init__(self, sensList = [0]*84):
        if len(sensList) == 84:
            self._sensList = sensList[:]
        else:
            raise ValueError('List used to build SensCollection should have 84 elements')       

        self._rangeType_dict = dict(r = _range_r, 
                                    phi = _range_phi,
                                    both = _range_both,
                                    )
        self._possibleMisalignements = []
        

    def __getitem__(self, key):
        '''use [] with my indexes (0-83) '''
        return self._sensList[key]

    def __setitem__(self, key, item):
        '''use [] with my indexes (0-83) '''
        self._sensList[key] = item

    def __call__(self, key):
        '''use () with experiment indexes (0-41; 64-105) '''
        if key in range(0,42)+range(64,106):
            return self._sensList[indMiei(key)]
        else:
            raise ValueError('Sensor number not valid: it must be between 0 and 41 or 64 and 105')

    def __add__(self, other):
        listResults = []
        for misSel, misOther in zip(self._sensList, other._sensList):
            listResults.append(misSel + misOther)
        return self.__class__(listResults)

    def __sub__(self, other):
        listResults = []
        for misSel, misOther in zip(self._sensList, other._sensList):
            listResults.append(misSel - misOther)
        return self.__class__(listResults)

    def __abs__(self):
        listResults = []
        for misSel in self._sensList:
            listResults.append(abs(misSel))
        return self.__class__(listResults)

 
    def getTGraphErrors(self, mis, show_sensors=None, isZPos=True):
        '''
        Give a pair of TGraphErrors: (mis_A or mis_C)
        mis in (dx, dy, dz, dalpha, dbeta, dgamma)
        show_sensors in ("r", "phi", "both")
        if isZPos==False plot as a function of sensor number
        '''
        
        try:         
            range_type = self._rangeType_dict[show_sensors] if show_sensors else self._rangeType_dict[mis]
        except KeyError:
            range_type = _rangeType()
  
        graphs = {}
        for side, start in {'A' : 0, 'C' : 1}.items():
            xValues = range_type.zPositions if isZPos else range_type.sensNums
            goodIndices = range(range_type.sensMax[0]+start, range_type.sensMax[1], 2)
            x  = array('d',[xValues[i] for i in goodIndices])
            ex = array('d',[0 for i in goodIndices])
            y =  array('d',[0 if not self._sensList[iModule] else getattr(self._sensList[iModule],mis)      for iModule in goodIndices])
            try:
                ey = array('d',[0 if not self._sensList[iModule] else getattr(self._sensList[iModule],'e'+mis)  for iModule in goodIndices])
            except AttributeError:
                # if error not found in the conventional way (es edx is error of dx) errors are set to zero
                ey = array('d',[0 for i in goodIndices])
            
            graphs[side] = TGraphErrors(len(x),x, y, ex, ey)

        return (graphs['A'], graphs['C'])
      
      
    def getTGraphErrorsDictionary(self, isZPos=True, **show_mis):
        '''
        Give a dictionary of TGraphErrors with keys mis_A or mis_C
        mis defined by the type of SensAbs, es for SensMis mis is in (dx, dy, dz, dalpha, dbeta, dgamma)
        if zPos==False plot as a function of sensor number
        show_dx etc must be called only to change the default and can be set to "r", "phi" or "both"        
        '''
        show_sensors_dict = {key : None for key in self._sensList[0].__dict__}
        #if show_mis:
        for show_key, val in show_mis.items():
            key = show_key.split('_')[1]
            show_sensors_dict[key] = val
        
        
        graphs = {}
        for mis in self._possibleMisalignements:
            graph = self.getTGraphErrors(mis, show_sensors_dict[mis], isZPos)
            graphs[mis+'_A'] = graph[0]
            graphs[mis+'_C'] = graph[1]
        return graphs


    # FIXME this is not general and holds only for sensMis!
    def getTH1D(self, mis, show_sensors=None, xmin=-6, xmax=6,nbins=25,label=''):

        try:         
            range_type = self._rangeType_dict[show_sensors] if show_sensors else self._rangeType_dict[mis]
        except KeyError:
            range_type = _rangeType()

        # To do make this more general
        titles = dict(dx = '#Delta_{x};#Delta_{x} [#mum];Entries',
                      dy = '#Delta_{y};#Delta_{y} [#mum];Entries',
                      dz = '#Delta_{z};#Delta_{z} [#mum];Entries',
                      dalpha = '#Delta_{#alpha};#Delta_{#alpha} [mrad];Entries',
                      dbeta = '#Delta_{#beta};#Delta_{#beta} [mrad];Entries',
                      dgamma = '#Delta_{#gamma};#Delta_{#gamma} [mrad];Entries')

        histo = TH1D(mis+label, titles[mis], nbins, xmin, xmax)
        for iModule in range(*range_type.sensMax):
            histo.Fill(getattr(self._sensList[iModule], mis))
        return histo


    def getTH1DDictionary(self, label, ranges_x={}, **show_mis):
        '''
        Give a dictionary of TH1D with keys mis
        mis in (dx, dy, dz, dalpha, dbeta, dgamma)
        show_dx etc must be called only to change the default and can be set to "r", "phi" or "both"        
        '''

        ranges_x_defaults = dict(
            dx = 6, dy =6, dz =200, dalpha = 5, dbeta =5, dgamma = 0.2)

        ranges_x = {key: ranges_x.get(key,ranges_x_defaults[key]) for key in ranges_x_defaults}

            
        show_sensors_dict = {key : None for key in self._sensList[0].__dict__}
        for show_key, val in show_mis.items():
            key = show_key.split('_')[1]
            show_sensors_dict[key] = val

        histos = {}
        for mis in self._possibleMisalignements:
            histo = self.getTH1D(mis, show_sensors=show_sensors_dict[mis], xmin=-1*ranges_x[mis],xmax=ranges_x[mis],label=label)
            histos[mis] = histo
        return histos


    
    
class SensMisCollection(SensAbsCollection):
    '''
    Class wich contains a ...
    '''
    
    def __init__(self, sensList = [0]*84, nullMis=False,inputXML = None):
        SensAbsCollection.__init__(self, sensList = sensList)
        self._rangeType_dict = dict(dx = _range_both,
                                    dy = _range_both,
                                    dz = _range_r,
                                    dalpha = _range_r,
                                    dbeta =  _range_r,
                                    dgamma = _range_phi,
                                    r = _range_r,
                                    phi = _range_phi,
                                    both = _range_both,)
        self._possibleMisalignements = ('dx', 'dy', 'dz', 'dalpha', 'dbeta', 'dgamma')
        if nullMis: self.fillWithZeroes()
        if inputXML != None: self.readFromXML(inputXML)

    def fillWithZeroes(self):
        """
        Fill with SensMis with zeroes misalignment
        """
        self._sensList = [SensMis(i) for i in range(42)+range(64,106)]
    
    def readFromTXT(self, file):
        misSens = [0]*84
        for i in range(84):
            misSens[i] = SensMis(i,1000,1000,1000,1000,1000,1000)
        for line in open(file):
            if (line.find('dx')<0):
                data=line.split('\t')
                numsens=int(data[0])
                dx=float(data[1])
                dy=float(data[2])
                dz=float(data[3])
                dalpha=float(data[4])
                dbeta=float(data[5])
                dgamma=float(data[6])
                edx=float(data[7])
                edy=float(data[8])
                edz=float(data[9])
                edalpha=float(data[10])
                edbeta=float(data[11])
                edgamma=float(data[12])
                misSens[indMiei(numsens)] = SensMis(numsens,dx,dy,dz,dalpha,dbeta,dgamma,edx,edy,edz,edalpha,edbeta,edgamma)
        self._sensList = misSens

    def readFromTXT_TxTy(self, file):
        misSens = [0]*84
        for line in open(file):
            if (line.find('dx')<0):
                data=line.split('\t')
                numsens=int(data[0])
                dx=float(data[1])
                dy=float(data[2])
                edx=float(data[3])
                edy=float(data[4])
                misSens[indMiei(numsens)] = SensMis(numsens,dx,dy,0.,0.,0.,0.,edx,edy,0.,0.,0.,0.)
        self._sensList = misSens


    def readFromXML(self, file, fileType='module'): # Ancora in fase di sviluppo, non si e` mai sicuri sui segni'
        '''
        fileType puo` essere module o sensor, a seconda di quello che e` assegna ai sensori il misalignment opportuno
        '''

        nameInXML = {'module' : 'Module', 'sensor' : 'Detector'}
        
        if file: # if no file in input do nothing
            misSens = [0]*84
            text = open(file).read()
            if '<DDDB>' not in text:
                text = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"><DDDB>\n'+text+'\n</DDDB>'
            root = xml.etree.ElementTree.fromstring(text)

            for module in root:
                if module.attrib['name'].find('PU') < 0 and module.attrib['name'].find(nameInXML[fileType]) >= 0: # no PU sensors
                    if fileType == 'module':
                        modNum = int(module.attrib['name'][-2:]) #Take module number
                    elif fileType == 'sensor':
                        sensNum = int(module.attrib['name'][-2:]) #Take sensor number
                        modNum = int(module.attrib['name'][-5:-3]) #Take module number
                    [Tx, Ty, Tz] = [float(i) for i in module[0].text.split()]
                    [Rx, Ry, Rz] = [float(i) for i in module[1].text.split()]

                    # Flip sign to go from local to global frame, should be correct
                    sgnX = 1 if modNum % 2 == 0 else -1
                    sgnY = (1 if modNum % 4 < 2 else -1)
                    if fileType == 'sensor' and indVeri(nsens=sensNum, nmod=modNum)>50:
                        sgnY *= -1
                    sgnZ = sgnX * sgnY
                    Tx, Ty, Tz = Tx * sgnX, Ty * sgnY, Tz *sgnZ
                    Rx, Ry, Rz = Rx * sgnX, Ry * sgnY, Rz *sgnZ

                    if fileType == 'module':
                        misSens[modNum] = SensMis(modNum,Tx,Ty,Tz,Rx,Ry,Rz)
                        misSens[modNum+42] = SensMis(modNum+64,Tx,Ty,Tz,Rx,Ry,Rz)
                    elif fileType == 'sensor':
                        misSens[indMiei(indVeri(sensNum, modNum))] = SensMis(indVeri(sensNum, modNum),Tx,Ty,Tz,Rx,Ry,Rz)
            self._sensList = misSens

    def writeToXML(self, outFile_name, fileType='module', oldFormat = False): 
        '''
        fileType puo` essere module o sensor, a seconda di quello che e` assegna ai sensori il misalignment opportuno
        '''
        inFile = '/afs/cern.ch/user/g/gdujany/LHCb/Alignment/alignScripts/sim_std/Conditions/Velo/Alignment/'+('Modules.xml' if fileType=='module' else 'Detectors.xml')
                
        if inFile: # if no file in input do nothing
            tree = xml.etree.ElementTree.parse(inFile)
            root = tree.getroot()
            
            for sensor in (self._sensList[:42] if fileType=='module' else self._sensList):
                nmod, nsens = getModSens(sensor.numsens)
                [Tx, Ty, Tz] = [sensor.dx/1000., sensor.dy/1000., sensor.dz/1000.]
                [Rx, Ry, Rz] = [sensor.dalpha/1000., sensor.dbeta/1000., sensor.dgamma/1000.]

                # Flip sign to go from local to global frame, should be correct
                sgnX = 1 if nmod % 2 == 0 else -1
                sgnY = (1 if nmod % 4 < 2 else -1)
                if fileType == 'sensor' and sensor.numsens>50:
                    sgnY *= -1
                sgnZ = sgnX * sgnY
                Tx, Ty, Tz = Tx * sgnX, Ty * sgnY, Tz *sgnZ
                Rx, Ry, Rz = Rx * sgnX, Ry * sgnY, Rz *sgnZ

                name2find = 'Module{nmod:02}' if fileType=='module' else 'Detector{nmod:02}-{nsens:02}'

                root.find("condition[@name='"+name2find.format(**locals())+"']/paramVector[@name='dPosXYZ']").text = "{} {} {}".format(Tx, Ty, Tz)
                root.find("condition[@name='"+name2find.format(**locals())+"']/paramVector[@name='dRotXYZ']").text = "{} {} {}".format(Rx, Ry, Rz)

            
            tree.write(outFile_name)
            outFile = open(outFile_name)
            text = outFile.read()
            outFile.close()
            outFile = open(outFile_name,'w')
            if oldFormat:
                outFile.write('<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">')
            else:
                text = text.replace('<DDDB>','').replace('</DDDB>','')
            outFile.write(text)
            outFile.close()

        else:
            raise IOError('Not found std xml file '+inFile)

                
                
    def __str__(self):
        return '\n'.join([i.__str__() for i in self._sensList])


### End Class SensMisCollection


class GlobalMis:

    def __init__(self, file=None):
        self._alignables = {}
        if file:
            self.readFromXML(file)
        else:
            for cont, key in enumerate(('VeloSystem', 'VeloLeft', 'VeloRight')):
                self._alignables[key] = SensMis(cont)

    def __getitem__(self, key):
        '''use [] possibles keys: VeloSystem, VeloLeft, VeloRight'''
        return self._alignables[key]

    def __setitem__(self, key, item):
        '''use [] possibles keys: VeloSystem, VeloLeft, VeloRight'''
        self._alignables[key] = item


    def __add__(self, other):
        somma = GlobalMis()
        for key in self._alignables:
            somma._alignables[key] = self[key]+other[key]
        return somma

    def __sub__(self, other):
        differenza = GlobalMis()
        for key in self._alignables:
            differenza._alignables[key] = self[key]-other[key]
        return differenza
        
    def __abs__(self):
        absolute = GlobalMis()
        for key in self._alignables:
            absolute._alignables[key] = abs(self._alignables[key])
        return absolute
            
    def keys(self):
        return self._alignables.keys()

    def items(self):
        return self._alignables.items()

    def values(self):
        return self._alignables.values()

    def readFromXML(self, file):
        if file:
            text = open(file).read()
            if '<DDDB>' not in text:
                text = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"><DDDB>\n'+text+'\n</DDDB>'
            root = xml.etree.ElementTree.fromstring(text)

            for cont, key in enumerate(('VeloSystem', 'VeloLeft', 'VeloRight')):
                [Tx, Ty, Tz] = [float(i) for i in root.find("condition[@name='"+key+"']/paramVector[@name='dPosXYZ']").text.split()]
                [Rx, Ry, Rz] = [float(i) for i in root.find("condition[@name='"+key+"']/paramVector[@name='dRotXYZ']").text.split()]
                self._alignables[key] = SensMis(cont,Tx,Ty,Tz,Rx,Ry,Rz)

    def writeToXML(self,outFile_name, oldFormat = False):
        inFile = os.path.join(this_dir ,'Global.xml')
        if inFile: # if no file in input do nothing
            tree = xml.etree.ElementTree.parse(inFile)
            root = tree.getroot()

            for key in ('VeloSystem', 'VeloLeft', 'VeloRight'):
                try:
                    [Tx, Ty, Tz] = [self[key].Tx/1000., self[key].Ty/1000., self[key].Tz/1000.,]
                    [Rx, Ry, Rz] = [self[key].Rx/1000., self[key].Ry/1000., self[key].Rz/1000.,]
                    root.find("condition[@name='"+key+"']/paramVector[@name='dPosXYZ']").text = "{} {} {}".format(Tx, Ty, Tz)
                    root.find("condition[@name='"+key+"']/paramVector[@name='dRotXYZ']").text = "{} {} {}".format(Rx, Ry, Rz)

                except KeyError:
                    pass
            tree.write(outFile_name)
            outFile = open(outFile_name)
            text = outFile.read()
            outFile.close()
            outFile = open(outFile_name,'w')
            if oldFormat:
                outFile.write('<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">')
            else:
                text = text.replace('<DDDB>','').replace('</DDDB>','')
            outFile.write(text)
            outFile.close()
                
        else:
            raise IOError('Not found std xml file '+inFile)
        

        
       

    def getSensMisCollection(self):
        '''
        Return a SensMisCollection with the reproduce the same pattern of the global misalignment
        '''
        # have to decide if I care about the VeloSystem wich is constant and zero a part for Rx and Rz
        # veloA = self['VeloSystem'] + self['VeloLeft']
        # veloC = self['VeloSystem'] + self['VeloRight']
        veloA = self['VeloLeft']
        veloC = self['VeloRight']
        misCollection = SensMisCollection()
        for i in range(42):
            globMis = veloA if i%2==0 else veloC
            dx = (globMis.dx + zPos[i]*sin(globMis.dbeta))/1000. # when put in SensMis must be in mm and rad
            dy = (globMis.dy - zPos[i]*sin(globMis.dalpha))/1000.
            dz = (globMis.dz - zPos[i]*(2 - cos(globMis.dalpha) - cos(globMis.dbeta)))/1000.
            misCollection[i] = SensMis(i,dx,dy,dz,globMis.dalpha/1000.,globMis.dbeta/1000.,globMis.dgamma/1000.)
            misCollection[i+42] = SensMis(i+64,dx,dy,dz,globMis.dalpha/1000.,globMis.dbeta/1000.,globMis.dgamma/1000.)
        return misCollection


    def __str__(self):
        return '\n'.join([key+':\n'+self[key].__str__() for key in ('VeloSystem', 'VeloLeft', 'VeloRight')])



        
### End Class GlobalMis

def findPath(file_names, startingPath):
    if isinstance(file_names, str):
        file_name = file_names
        for root, dirs, files in os.walk(startingPath):
            for file in files:
                if fnmatch.fnmatch(file, file_name):
                    return root+'/'+file
    else:
        for file_name in file_names:
            for root, dirs, files in os.walk(startingPath):
                for file in files:
                    if fnmatch.fnmatch(file, file_name):
                        return root+'/'+file
   

class VeloMis:
    '''
    Class wich contains all Velo mis:
    globMis, modmis and sensMis.
    It can read from a folder where there are global.xml, Modules.xml and Sensors.xml
    or one can give the 3 files (or folders wich contain them) separately
    it also have defined sum, difference and abs
    '''

    def __init__(self,folder=None, globMis=None, modMis=None, sensMis=None, oldDB=False):
        self.globMis = GlobalMis()
        self.modMis = SensMisCollection()
        self.sensMis = SensMisCollection()
        self.readFromXML(folder=folder, globMis=globMis, modMis=modMis, sensMis=sensMis, oldDB=oldDB)

    def readFromXML(self,folder=None, globMis=None, modMis=None, sensMis=None, oldDB=False):
        if folder != None:
            
            self.globMis.readFromXML(findPath(['Global.xml', 'VeloGlobal.xml'],folder))
            self.modMis.readFromXML(findPath(['Modules.xml','VeloModules.xml'],folder), fileType='module')
            sensFile_name = ['Detectors.xml', 'VeloDetectors.xml'] if oldDB else ['Modules.xml', 'VeloModules.xml']
            self.sensMis.readFromXML(findPath(sensFile_name,folder), fileType='sensor')
            
        if globMis != None:
            if globMis[-4:] == '.xml':
                self.globMis.readFromXML(globMis)
            else:
                self.globMis.readFromXML(findPath(['Global.xml', 'VeloGlobal.xml'],globMis))

        if modMis != None:
            if modMis[-4:] == '.xml':
                self.modMis.readFromXML(modMis)
            else:
                self.modMis.readFromXML(findPath(['Modules.xml','VeloModules.xml'],modMis))

        if sensMis != None:
            if sensMis[-4:] == '.xml':
                self.sensMis.readFromXML(sensMis,fileType='sensor')
            else:
                sensFile_name = ['Detectors.xml', 'VeloDetectors.xml'] if oldDB else ['Modules.xml', 'VeloModules.xml']
                self.sensMis.readFromXML(findPath(sensFile_name,sensMis),fileType='sensor')

    def writeToXML(self, folder='.', makeSubFolders=True, oldFormat = False):
        if folder[-1] != '/': folder += '/'
        if makeSubFolders: folder += 'Conditions/Velo/Alignment/'
        os.system('mkdir -p '+folder)
        self.globMis.writeToXML(folder+'Global.xml', oldFormat=oldFormat)
        self.modMis.writeToXML(folder+'Modules.xml', fileType='module', oldFormat=oldFormat)
        self.sensMis.writeToXML(folder+'Detectors.xml', fileType='sensor', oldFormat=oldFormat)
    
    
    def __add__(self, other):
        somma = VeloMis()
        for mis in ['globMis','modMis','sensMis']:
            exec 'somma.'+mis+'= getattr(self,mis)+getattr(other,mis)'
        return somma

    def __sub__(self, other):
        differenza = VeloMis()
        for mis in ['globMis','modMis','sensMis']:
            exec 'differenza.'+mis+'= getattr(self,mis)-getattr(other,mis)'
            #getattr(differenza,mis) = getattr(self,mis)-getattr(other,mis)
        return differenza
        
    def __abs__(self):
        absolute = VeloMis()
        for mis in ['globMis','modMis','sensMis']:
            exec 'absolute.'+mis+'= abs(getattr(self,mis))'
            #getattr(absolute,mis) = abs(getattr(self,mis))
        return absolute

    def normalize(self, moveVeloSys=True):
        """
        Changes the alignment constants so that the Tx Ty Tz Rz mean of modules in an half is 0 and
        all dofs of the two alves also are in average zero.
        """
        meanLeft = sum(self.modMis[0:84:2],SensMis(0))/42.
        meanRight = sum(self.modMis[1:84:2],SensMis(0))/42.

        for mis in ('dalpha', 'dbeta', 'edx', 'edy', 'edz', 'edalpha', 'edbeta', 'edgamma'):
            meanLeft.__dict__[mis] = 0.
            meanRight.__dict__[mis] = 0.

        for i in range(0,84,2):
            self.modMis[i] -= meanLeft

        for i in range(1,84,2):
            self.modMis[i] -= meanRight

        self.globMis['VeloLeft'] += meanLeft
        self.globMis['VeloRight'] += meanRight

        meanVelo = (self.globMis['VeloLeft']+self.globMis['VeloRight'])/2.

        self.globMis['VeloLeft'] -= meanVelo
        self.globMis['VeloRight'] -= meanVelo
        if moveVeloSys:
            # Leaving VeloSystem unchanged is probably wrong but keep same position along the data taking so it may be more consistent
            # some more though is needed
            self.globMis['VeloSystem'] += meanVelo 

    def getSensMisCollection(self):
        '''
        Return a SensMisCollection with the reproduce the same pattern of the global+module+sensor misalignment
        '''
        return self.globMis.getSensMisCollection() + self.modMis + self.sensMis



### End Class VeloMis
   
def drawGraphsSens(h_dict, var_dict, rangeY_dict={}, title='', separeHalves=True, onlyAside=False, isZPos=True, **show_mis):
    '''
    Function to ...
    h_dict is dictionary with: {label : SensAbsCollection}
    var_dict is a dict with  vars to compare: it can be:
    {var: (var_name, var_unit)} -> axes label will be "var name [var unit]"
    {var: var_name} -> no var unit to put between brackets

    rangeY_dict is dict with {var: (minY, maxY)}, if a var is not in dict default range will be assigned, if minY or maxY are None, a default value will be assigned.
    
    show_dx etc must be called only to change the default and can be set to "r", "phi" or "both"

    return a dictionary of MultiPlos, that contains already the legend
    '''

    mis_list = var_dict.keys()

    z_label = 'z [mm]' if isZPos else 'sensor number'

    if len(h_dict) == 1: title += h_dict.keys()[0]
    hs = {}
    for var, value in var_dict.items():
        if type(value) == tuple or type(value) == list:
            name = value[0]
            unit = ' ['+str(value[1])+']'
        else:
            name = value
            unit = ''

        hs[var] = MultiPlot(
            'hs_'+var, kind='g',
            title = title + ' ' + name +';'+z_label+';'+name+unit,
            rangeY = rangeY_dict.get(var,(None,None)),
        )
        
   
    graphs = {}
    for cont, (label, sensMisColl) in enumerate(sorted(h_dict.items())):
        graphs[label] = sensMisColl.getTGraphErrorsDictionary(isZPos, **show_mis)
        for var in var_dict:
            labels = ('A-side', 'C-side') if len(h_dict) == 1 else (label,None)
            hs[var].Add(graphs[label][var+'_A'],labels[0],cont+1)
            if not onlyAside:
                style = -(cont+1) if separeHalves else cont+1
                hs[var].Add(graphs[label][var+'_C'],labels[1],style)

    return hs

    

def drawGraphsMis(h_dict, mis_list=None, rangeY_dict={}, title='', separeHalves=True, onlyAside=False, isZPos=True, **show_mis):
    '''
    Function to ...
    h_dict is dictionary with: {label : SensMisCollection}
    mis_list is a list with the misalignments to compare, mis in (dx, dy, dz, dalpha, dbeta, dgamma)
    show_dx etc must be called only to change the default and can be set to "r", "phi" or "both"
    '''
    
    if not mis_list:
        mis_list = ['dx','dy']
        
    mis_dict_full = dict(dx = ('T_{x}', '#mum'),
                         dy = ('T_{y}', '#mum'),
                         dz = ('T_{z}', '#mum'),
                         dalpha = ('R_{x}', 'mrad'),
                         dbeta = ('R_{y}', 'mrad'),
                         dgamma = ('R_{z}', 'mrad'),)
    
    var_dict = {key:mis_dict_full[key] for key in mis_list}
    return drawGraphsSens(h_dict=h_dict, var_dict=var_dict, rangeY_dict=rangeY_dict, title=title, separeHalves=separeHalves, onlyAside=onlyAside, isZPos=isZPos, **show_mis)



def drawTH1DMis(h_dict, mis_list=None, title='',  **show_mis):

    if not mis_list:
        mis_list = ['dx','dy']

    mis_dict_full = dict(dx = ('T_{x}', '#mum'),
                         dy = ('T_{y}', '#mum'),
                         dz = ('T_{z}', '#mum'),
                         dalpha = ('R_{x}', 'mrad'),
                         dbeta = ('R_{y}', 'mrad'),
                         dgamma = ('R_{z}', 'mrad'),)

    # mis_dict_full = dict(dx = ('#Delta_{x}', '#mum'),
    #                      dy = ('#Delta_{y}', '#mum'),
    #                      dz = ('#Delta_{z}', '#mum'),
    #                      dalpha = ('#Delta_{#alpha}', 'mrad'),
    #                      dbeta = ('#Delta_{#beta}', 'mrad'),
    #                      dgamma = ('#Delta_{#gamma}', 'mrad'),)

    var_dict = {key:mis_dict_full[key] for key in mis_list}

    if len(h_dict) == 1: title += h_dict.keys()[0]
    hs = {}
    for var, value in var_dict.items():
        if type(value) == tuple or type(value) == list:
            name = value[0]
            unit = ' ['+str(value[1])+']'
        else:
            name = value
            unit = ''

        hs[var] = MultiPlot(
            'hs_'+var, kind='h',
            title = title + ' ' + name +';'+name+unit+';Entries',
            )
    
        
    graphs = {}
    for cont, (label, sensMisColl) in enumerate(h_dict.items()):
        graphs[label] = sensMisColl.getTH1DDictionary(label, **show_mis)
        for var in mis_list:
            hs[var].Add(graphs[label][var],label,cont+1)
            
    return hs


    
# Backup

# def drawGraphsSens(h_dict, var_dict, title='', logy=False, squarePad=False, separeHalves=True, onlyAside=False, isZPos=True, **show_mis):
#     '''
#     Function to ...
#     h_dict is dictionary with: {label : SensAbsCollection}
#     var_dict is a dict with  vars to compare: it can be:
#     {var: (var_name, var_unit)} -> axes label will be "var name [var unit]"
#     {var: var_name} -> no var unit to put between brackets
#     show_dx etc must be called only to change the default and can be set to "r", "phi" or "both"
#     '''

#     mis_list = var_dict.keys()

#     z_label = 'z [mm]' if isZPos else 'sensor number'

#     titles = {}
#     for var, value in var_dict.items():
#         if type(value) == tuple or type(value) == list:
#             name = value[0]
#             unit = ' ['+str(value[1])+']'
#         else:
#             name = value
#             unit = ''
#         titles[var] = title + ' ' + name +';'+z_label+';'+name+unit

#     c = {}
#     for mis in mis_list:
#         if squarePad:
#             c[mis] = TCanvas('c_'+mis, 'c',700,700)
#         else:
#             c[mis] = TCanvas('c_'+mis, 'c')
#         c[mis].hs = TMultiGraph('hs_'+mis, titles[mis])
#         c[mis].leg = TLegend(.80, 0.80, .99, .99,"")
#         c[mis].gr_list = []

#     graphs = {}
#     for cont, (label, sensMisColl) in enumerate(h_dict.items()):
#         graphs[label] = sensMisColl.getTGraphErrorsDictionary(isZPos, **show_mis)
#         for mis in mis_list:
#             setPlotAttributes(graphs[label][mis+'_A'], colori[cont], markersA[cont])
#             if separeHalves:
#                 setPlotAttributes(graphs[label][mis+'_C'], colori[cont], markersC[cont])
#             else:
#                 setPlotAttributes(graphs[label][mis+'_C'], colori[cont], markersC[cont])
#             hh = graphs[label][mis+'_A'].Clone()
#             c[mis].hs.Add(hh)
#             c[mis].gr_list.append(hh)
#             if not onlyAside:
#                 hh = graphs[label][mis+'_C'].Clone()
#                 c[mis].hs.Add(hh)
#                 c[mis].gr_list.append(hh)
#             c[mis].leg.AddEntry(graphs[label][mis+'_A'],label,'p')

#     for mis in mis_list:
#         c[mis].cd()
#         if logy: c2[mis].SetLogy()
#         c[mis].hs.Draw('ap')#('nostack')
#         c[mis].leg.Draw('same')
#         c[mis].leg.SetFillColor(0)
#         c[mis].Update()  
#     return c                        
