#!/usr/bin/env python

import sys, os, argparse, fnmatch, re

parser = argparse.ArgumentParser(description ='Macro to automatise the analysis of a new onlin alignment')

parser.add_argument('run', help='run number')
parser.add_argument('-y','--notFixRangeY',help='do not fix y-range to default values',action='store_true')
# group = parser.add_mutually_exclusive_group()
parser.add_argument('-p','--plotsDir',help='create folder with a pdf for each plot',nargs='?',const='plots')
parser.add_argument('-d',help='input directory with the runXXX')
parser.add_argument('-t','--Tracker', help='Analise Tracker instead of Velo',action='store_true')
args = parser.parse_args()

# if not os.path.exists(args.directory):
#     raise IOError('Input directory does not exists')

fixY_str = '' if args.notFixRangeY else ' -y'
activity = 'Velo'

if args.Tracker:
    activity = 'Tracker'
    if not args.d:
        args.d = '/group/online/AligWork/Tracker/'
        
if not args.d:
    args.d = '/group/online/AligWork/Velo/'
# inDir = args.directory[:-1] if args.directory[-1] == '/' else args.directory
inDir = os.path.join(args.d, 'run{0}'.format(args.run))
label = os.path.split(inDir)[-1]
outDir = 'SynthesisAlignment{0}_{1}/'.format(activity, label)
run = args.run

if not os.path.exists(outDir):
    os.mkdir(outDir)

print 'making plots alignlog'
outFile = os.path.join(outDir, 'plotsIterations')
plotsDir_label = '-d {0}{1}/plotsIterations'.format(outDir, args.plotsDir) if args.plotsDir else ''
print 'plotAlignlog.py -o {outFile} {label}:{inDir} {plotsDir_label}'.format(**globals())
os.system('plotAlignlog.py -o {outFile} {label}:{inDir} {plotsDir_label}'.format(**globals()))


if activity == 'Velo':
    print 'making plots to compare initial and final alignment constants'
    outFile = os.path.join(outDir, 'plotsXML')
    plotsDir_label = '-d {0}{1}/plotsXML'.format(outDir, args.plotsDir) if args.plotsDir else ''
    os.system('compareXML.py --noStd -y -o {outFile} initial:"{inDir}/Iter1" final:"{inDir}/xml" {plotsDir_label}'.format(**globals()))

if activity == 'Velo':
    print 'making plots to compare alignment constants of various iterations'
    outFile = os.path.join(outDir, 'plotsXMLIterations')
    plotsDir_label = '-d {0}{1}/plotsXMLIterations'.format(outDir, args.plotsDir) if args.plotsDir else ''
    command = 'compareXML.py --noStd -o '+outFile
    for iter in sorted([ i for i in os.listdir(inDir) if 'Iter' in i]):
        command += ' {0}:"{1}/{0}"'.format(iter, inDir)
    command += ' {0}:"{1}/{0}" '.format('xml', inDir)
    command += plotsDir_label
    os.system(command)


try:
    print 'Run macro Wouter'
    histDir = '/hist/Savesets/2015/LHCbA/AligWrk_{0}'.format(activity)
    matches = []
    for root, dirs, files in os.walk(histDir):
        for file in files:
            match = re.findall(r'AligWrk_{activity}-{run}(.*?)-(.*?)-EOR.root'.format(**locals()), file)
            if match:
                matches.append(list(match[0])+[os.path.join(root, file)])

    matches = sorted(matches, key=lambda x: x[1], reverse = True)

    outFile = os.path.join(outDir, 'plotsMonitoring.pdf')
    command = 'runMacroWouter.py -o {0} '.format(outFile)
    if matches[0][0] == '01': # only one histogram file
        command += 'old:'+matches[0][2]
    else:
        for match in matches:
            if match[0] == '01':
                command += 'old:"{0}" new:"{1}"'.format(match[2], matches[0][2])
                break
    if 'old' not in command: # temporary patch if first run has not 01, use only timestamp info, assumes each fill run only once
        command += 'old:"{0}" new:"{1}"'.format(matches[-1][2], matches[0][2])
    print command
    os.system(command)



    if activity == 'Velo':
        print 'Make plots overlap residuals'
        outFile = os.path.join(outDir, 'plots_summaryOverlap.pdf')
        command = 'gSummaryOverlap.py -o {0} {1} '.format(outFile, fixY_str)
        if matches[0][0] == '01':
            command += 'old:'+matches[0][2]
        else:
            for match in matches:
                if match[0] == '01':
                    command += 'old:"{0}" new:"{1}"'.format(match[2], matches[0][2])
                    break
        if 'old' not in command: # temporary patch if first run has not 01, use only timestamp info, assumes each fill run only once
            command += 'old:"{0}" new:"{1}"'.format(matches[-1][2], matches[0][2])
        print command
        os.system(command)

except IndexError:
    pass

# Find big changes
print '\nFind big changes'

from ConstantsReferences import *
dofs = ['Tx', 'Ty', 'Tz', 'Rz', 'Ry', 'Rz']
os.system('echo "Big changes\n" > {outDir}/changes.txt'.format(**globals()))
os.system('echo "Alert changes\n" > {outDir}/alertChanges.txt'.format(**globals()))
for group in globals()[activity]:
    regex, file_path, diffMin, diffMax = group
    os.system('echo "\nBig changes {regex}" >> {outDir}/changes.txt'.format(**globals()))
    limits_str = ' '.join(['{0}:{1}'.format(dof, diff) for dof, diff in zip(dofs, diffMin)])
    os.system('diffXML.py {inDir}/Iter1/{file_path} {inDir}/xml/{file_path} --regex "{regex}" --limits {limits_str} >> {outDir}/changes.txt'.format(**locals()))

    os.system('echo "\nAlert changes {regex}" >> {outDir}/alertChanges.txt'.format(**globals()))
    limits_str = ' '.join(['{0}:{1}'.format(dof, diff) for dof, diff in zip(dofs, diffMax)])
    os.system('diffXML.py {inDir}/Iter1/{file_path} {inDir}/xml/{file_path} --regex "{regex}" --limits {limits_str} >> {outDir}/alertChanges.txt'.format(**locals()))

                  
