#!/usr/bin/env python
"""
Wrapper to Wouter's script to plot statndard histos
es.
./runMacroWouter.py beamgas:../align2halves/AlignmentResults/Iter0/histograms.root collisions:../../recoStd/stdAlign_fill2239-200000ev-histos.root -o plots_compareReco.pdf -n
"""
import argparse, sys, os
this_files_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description ="Wrapper to Wouter's script to plot standard histos")
parser.add_argument('files',help='''files to process, max 4,
es label1:file1.root  label2:file2.root, max 4 files''', nargs='*', type=str)
parser.add_argument('-n','--normalize',help='Histograms should be normalized',action='store_true')
parser.add_argument('-o',help='output file name')
parser.add_argument('-i','--online',help='in oline network accept iteration folder and guess position histograms', action='store_true')
args = parser.parse_args()

if not args.files:
    print 'No files to process'
    sys.exit(1)
if len(args.files) > 4:
    print 'To many inputs files, will consider only first 4'

if args.online:
    from getPathHistoOnline import getPathHistoOnline
    args.files = [':'.join([i.split(':')[0], getPathHistoOnline(i.split(':')[1])]) for i in args.files]
    

import ROOT as r

r.gROOT.SetBatch(True)
r.gROOT.ProcessLine('.L {0}/macroWouter/drawutils.cxx+'.format(this_files_dir))
r.gROOT.ProcessLine('.L {0}/macroWouter/drawalignmon.cxx'.format(this_files_dir))
listFiles = ['"'+i+'"' for i in args.files]+['""' for i in range(4-len(args.files))]
r.gROOT.ProcessLine('drawall({1},{2},{3},{4}{0})'.format(',1' if args.normalize else '',*listFiles))

if args.o:
    os.rename('plots.pdf', args.o)
os.remove('plots.ps')
