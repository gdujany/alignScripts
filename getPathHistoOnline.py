#!/usr/bin/env python

import os, time

def getPathHistoOnline(iterDir, activity=None):
    '''
    Given a folder with an iteration returns corresponding histogram
    '''

    if activity == None:
        absPath = os.path.abspath(iterDir)
        if 'Velo' in absPath: activity = 'Velo'
        elif 'Tracker' in absPath: activity = 'Tracker'
        elif 'Muon' in absPath: activity = 'Muon'
    
    iterTime = os.path.getmtime(iterDir)
    iterTime_tuple = time.localtime(iterTime)
    year = time.strftime('%Y' ,iterTime_tuple)
    month = time.strftime('%m',iterTime_tuple)
    day = time.strftime('%d',iterTime_tuple)

    histDir = '/hist/Savesets/{year}/LHCbA/AligWrk_{activity}/{month}/{day}'.format(**locals())

    candidates = [os.path.join(histDir,i) for i in os.listdir(histDir) if abs(os.path.getmtime(os.path.join(histDir,i)) - iterTime) < 60 ]

    if len(candidates) == 0:
        raise IOError('No histogram found corresponding to '+iterDir)
    elif len(candidates) > 1:
        raise IOError(len(candidates)+' histogram found corresponding to '+iterDir)

    return candidates[0]

if __name__ == '__main__':

        import argparse
        parser = argparse.ArgumentParser(description ="From folder iteration provide path histogram on online system")
        parser.add_argument('iterDir',help='''iteration directory''')
        args = parser.parse_args()

        print getPathHistoOnline(args.iterDir)

