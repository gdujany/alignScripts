#!/usr/bin/env python
'''
Macro to make plots with overlap hits from standard histograms
es.
./gSummaryOverlap.py iter0:AlignmentResults/Iter0/histograms.root iter5:AlignmentResults/Iter5/histograms.root -o test15.pdf
'''

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make plots with overlap hits from standard histograms")
    parser.add_argument('files',help='''files to process, max 4,
    es label1:file1.root  label2:file2.root''', nargs='*', type=str)
    parser.add_argument('-o','--outFile',help='output file name',default='summaryOverlap.pdf')
    parser.add_argument('-y','--fixRangeY',help='fix y-range to default values',action='store_true')
    parser.add_argument('--rangeR',help='fix y-range of dR plots +- value given',type=float)
    parser.add_argument('--rangePhi',help='fix y-range of dPhi plots +- value given',type=float)
    parser.add_argument('-n','--numSens',help='Use sensor number instead of z-position',action='store_true')
    args = parser.parse_args()

##########################
    
import sys
sys.path.append('/afs/cern.ch/user/g/gdujany/LHCb/Alignment/newCode')
sys.path.append('/afs/cern.ch/user/g/gdujany/pyUtils')
from sensorUtils import *
from MultiPlot import MultiPlot

import ROOT as r

r.gStyle.SetGridColor(16)

class SensOverlapMis(SensAbs):
    def __init__(self, numsens, dR, dPhi, edR, edPhi, nEntR, nEntPhi, bm_dx, bm_dy, um_dx, um_dy):
        vars_dict = dict(
            dR = dR*1000,  #*1000 per passare da mm a um e da rad a mrad
            dPhi = dPhi*1000,
            edR = edR*1000,
            edPhi = edPhi*1000,
            nEntR = nEntR,
            nEntPhi = nEntPhi,
            b_dR = (dR-bm_dy)*1000,
            b_dPhi = (dPhi-bm_dx)*1000,
            u_dR = (dR-um_dy)*1000,
            u_dPhi = (dPhi-um_dx)*1000,
            eb_dR = edR*1000,
            eb_dPhi = edPhi*1000,
            eu_dR = edR*1000,
            eu_dPhi = edPhi*1000,)
        SensAbs.__init__(self, numsens, vars_dict)

    @staticmethod
    def _differenza(my_vars, other_vars):
        ret_dict = {}
        for var in ('dR', 'dPhi', 'nEntR', 'nEntPhi', 'b_dR', 'b_dPhi', 'u_dR', 'u_dPhi'):
            ret_dict[var] = my_vars[var] - other_vars[var]
        for var in ('edR', 'edPhi', 'eb_dR', 'eb_dPhi', 'eu_dR', 'eu_dPhi'):
            ret_dict[var] = sqrt(my_vars[var]**2 + other_vars[var]**2)
        return ret_dict


class SensOverlapMisCollection(SensAbsCollection):
    def __init__(self, sensList = [0]*84):
        SensAbsCollection.__init__(self, sensList=sensList)
        self._rangeType_dict['dR'] = self._rangeType_dict['r']
        self._rangeType_dict['dPhi'] = self._rangeType_dict['r']
        self._rangeType_dict['nEntR'] = self._rangeType_dict['r']
        self._rangeType_dict['nEntPhi'] = self._rangeType_dict['r']
        
        self._possibleMisalignements = ['dR', 'dPhi', 'nEntR', 'nEntPhi', 'u_dR', 'u_dPhi', 'b_dR', 'b_dPhi']
        
def getMisFromHistOverlap(histo, isFit):
    try:
        mean = histo.GetMean()
        emean=histo.GetMeanError()
        nentries = histo.GetEntries()
        if isFit:
            fit_fun = r.TF1("mygaus","gaus",-0.1,0.1)
            histo.Fit( fit_fun,"QNOE")
            pars = fit_fun.GetParameters()
            parerrs = fit_fun.GetParErrors()
            mean, emean = pars[1], parerrs[1]
        return mean, emean, nentries
    except AttributeError:
        return 0, 0, 0

def makeMis(inFile, num,  bm_dx, bm_dy, um_dx, um_dy, tb, isFit):
    station_number = int(num/2)
    histo_name = 'Track/TrackVeloOverlapMonitor/station{0}{1}/overlapResidual'.format(station_number, tb)
    dR, edR, nEntR = getMisFromHistOverlap(inFile.Get(histo_name+'R'),isFit)
    dPhi, edPhi, nEntPhi = getMisFromHistOverlap(inFile.Get(histo_name+'Phi'),isFit)
    return SensOverlapMis(num, dR, dPhi, edR, edPhi, nEntR, nEntPhi, bm_dx, bm_dy, um_dx, um_dy)

def getMeanMis(inFile, isFit):
    bm_dx , ebm_dx, tmp  = getMisFromHistOverlap(inFile.Get('Track/TrackVertexMonitor/PV left-right delta x'),isFit)
    bm_dy , ebm_dy, tmp = getMisFromHistOverlap(inFile.Get('Track/TrackVertexMonitor/PV left-right delta y'),isFit)
    um_dx , eum_dx, tmp = getMisFromHistOverlap(inFile.Get('Track/TrackPV2HalfAlignMonitor/Left-Right PV delta x'),isFit)
    um_dy , eum_dy, tmp = getMisFromHistOverlap(inFile.Get('Track/TrackPV2HalfAlignMonitor/Left-Right PV delta y'),isFit)
    return bm_dx, bm_dy, um_dx, um_dy
    

def makeMisColl(inFile_name, tb='Top', isFit=True):
    inFile = r.TFile(inFile_name)
    misColl = SensOverlapMisCollection()
    bm_dx, bm_dy, um_dx, um_dy = getMeanMis(inFile, isFit)
    for num in range(0,41,2):
        misColl[num] = makeMis(inFile, num, bm_dx, bm_dy, um_dx, um_dy, tb, isFit)
    return misColl

def makeMisDict(dict_files):
    misTop = {}
    misBot = {}
    for key, inFile_name in dict_files.items():
        misTop[key] = makeMisColl(inFile_name,'Top')
        misBot[key] = makeMisColl(inFile_name,'Bot')
    return misTop, misBot

def CompareOverlap(misTop, misBot, outFile_name, isZPos=True, title='', rangeY_dict = {}, alsoAverage=False, misTop_noDiff=None, misBot_noDiff=None):
        """
        dict files is of the kind {label:file_name}

        alsoAverage==True make also plot with average between top and botom weighted with number of entries,
        if misTop and misBottom have difference of number of Entries and not absolute value also misTop_noDiff and misBot_noDiff must be provided wich contain ...
        """

        dict_title_parts = dict(
            dR = ('Res R',' [#mum]'),
            dPhi = ('Res #Phi',' [#mum]'),
            nEntR = ('# ent R', ''),
            nEntPhi = ('# ent #Phi', ''),
            b_dR = ('Res R - biased average',' [#mum]'),
            b_dPhi = ('Res #Phi - biased average',' [#mum]'),
            u_dR = ('Res R - unbiased average',' [#mum]'),
            u_dPhi = ('Res #Phi - unbiased average',' [#mum]'), 
            )
        z_label = 'z [mm]' if isZPos else 'sensor number'

                
        graphsTop = {key : misTop[key].getTGraphErrorsDictionary(isZPos) for key in misTop}
        graphsBot = {key : misBot[key].getTGraphErrorsDictionary(isZPos) for key in misBot}

        
                
        if len(graphsTop) == 1: title += graphsTop.keys()[0]
        hs = {}
        def DrawRPhi(coso, firstpad):
            for pad, var in enumerate(['R', 'Phi'],firstpad):
                canvas.cd(pad)
                name = dict_title_parts[coso+var][0]
                unit = dict_title_parts[coso+var][1]
                hs[coso+var] = MultiPlot(
                    'hs_'+coso+'_'+var, kind='g',
                    title = title+' '+ name +';'+z_label+';'+name+unit,
                    rangeY=rangeY_dict.get(coso+var,(None,None)))
                for cont, key in enumerate(graphsBot):
                    labels = ('Top', 'Bottom') if len(graphsTop) == 1 else (key,None)
                    hs[coso+var].Add(graphsTop[key][coso+var+'_A'], labels[0], cont+1)
                    hs[coso+var].Add(graphsBot[key][coso+var+'_A'], labels[1], -(cont+1))
                hs[coso+var].Draw()

        for firstpad, coso in enumerate(('nEnt', 'd'),1):
            DrawRPhi(coso, 2*firstpad-1)
            canvas.Update()
        canvas.Print(outFile_name)
        for firstpad, coso in enumerate(('b_d', 'u_d'),1):
            DrawRPhi(coso, 2*firstpad-1)
            canvas.Update()
        canvas.Print(outFile_name)


        # Make and draw graphs Average and difference
        if alsoAverage:

            if misTop_noDiff == None: misTop_noDiff = misTop
            if misBot_noDiff == None: misBot_noDiff = misBot

            graphsTop_noDiff = {key : misTop_noDiff[key].getTGraphErrorsDictionary(isZPos) for key in misTop_noDiff}
            graphsBot_noDiff = {key : misBot_noDiff[key].getTGraphErrorsDictionary(isZPos) for key in misBot_noDiff}
            
            graphsAverage = {}
            graphsDifferences = {}
            for key in graphsTop:
                graphsAverage[key] = {}
                graphsDifferences[key] = {}
                for var in ('R', 'Phi'):
                    graphsAverage[key]['d'+var] = graphsTop[key]['d'+var+'_A'].Clone(key+'d'+var+'_average')
                    graphsDifferences[key]['d'+var] = graphsTop[key]['d'+var+'_A'].Clone(key+'d'+var+'_difference')
                    for iBin in range(graphsAverage[key]['d'+var].GetN()):
                        try:
                            graphsAverage[key]['d'+var].SetPoint(iBin,
                                                                 graphsAverage[key]['d'+var].GetX()[iBin] ,
                                                                 (graphsTop[key]['d'+var+'_A'].GetY()[iBin]*graphsTop_noDiff[key]['nEnt'+var+'_A'].GetY()[iBin] +
                                                                  graphsBot[key]['d'+var+'_A'].GetY()[iBin]*graphsBot_noDiff[key]['nEnt'+var+'_A'].GetY()[iBin] ) / (graphsTop_noDiff[key]['nEnt'+var+'_A'].GetY()[iBin] + graphsBot_noDiff[key]['nEnt'+var+'_A'].GetY()[iBin]) )
                        except ZeroDivisionError:
                            graphsAverage[key]['d'+var].SetPoint(iBin,
                                                             graphsAverage[key]['d'+var].GetX()[iBin] ,
                                                             0)
                        graphsAverage[key]['d'+var].SetPointError(iBin, 0, sqrt(graphsTop[key]['d'+var+'_A'].GetErrorY(iBin)**2 + graphsBot[key]['d'+var+'_A'].GetErrorY(iBin)**2))
                        
                        graphsDifferences[key]['d'+var].SetPoint(iBin,
                                                             graphsAverage[key]['d'+var].GetX()[iBin] , graphsTop[key]['d'+var+'_A'].GetY()[iBin] - graphsBot[key]['d'+var+'_A'].GetY()[iBin] )
                        graphsDifferences[key]['d'+var].SetPointError(iBin, 0, sqrt(graphsTop[key]['d'+var+'_A'].GetErrorY(iBin)**2 + graphsBot[key]['d'+var+'_A'].GetErrorY(iBin)**2))
                        

            multiPlots_average = {}
            multiPlots_difference = {}
            for var in ('R', 'Phi'):
                name = dict_title_parts['d'+var][0]
                unit = dict_title_parts['d'+var][1]
                multiPlots_average['d'+var] = MultiPlot(
                    'hs_average'+'_d'+var, kind='g',
                    title = title+' '+ name  +' average;'+z_label+';'+name+unit,
                    rangeY = rangeY_dict.get('d'+var,(None,None)),
                    histos = {key : graphsAverage[key]['d'+var] for key in graphsAverage},
                    labels = {key : 'Average' for key in graphsAverage} if len(graphsTop) == 1 else {key : key for key in graphsAverage},
                    )
                multiPlots_difference['d'+var] = MultiPlot(
                    'hs_difference'+'_d'+var, kind='g',
                    title = title+' '+ name  +' Top - Bottom;'+z_label+';'+name+unit,
                    rangeY = rangeY_dict.get('d'+var,(None,None)),
                    histos = {key : graphsDifferences[key]['d'+var] for key in graphsDifferences},
                    labels = {key : 'Top - Bottom' for key in graphsDifferences} if len(graphsTop) == 1 else {key : key for key in graphsDifferences},
                    )

            canvas.cd(1)
            multiPlots_average['dR'].Draw()
            canvas.cd(2)
            multiPlots_average['dPhi'].Draw()
            canvas.cd(3)
            multiPlots_difference['dR'].Draw()
            #hs['dR'].Draw()
            canvas.cd(4)
            multiPlots_difference['dPhi'].Draw()
            #hs['dPhi'].Draw()
            canvas.Print(outFile_name)
        
            

        

if __name__ == '__main__':

    r.gROOT.SetBatch()

    isZPos = not args.numSens

    canvas = TCanvas('canvas', 'canvas')
    outFile_name = args.outFile
    canvas.Print(outFile_name+'[')
    canvas.Divide(2,2)
    for i in range(5):
        canvas.GetPad(i).SetGrid()
    list_files = [i.split(':') for i in args.files]

    rangeR = args.rangeR if args.rangeR else 10#200#20
    rangePhi = args.rangePhi if args.rangePhi else 25#300#50


    #rangeY_dict = {'nEntR':(0,None), 'nEntPhi':(0,None), 'dR':(-10,10), 'dPhi':(-25,25), 'b_dR':(-10,10), 'b_dPhi':(-25,25), 'u_dR':(-10,10), 'u_dPhi':(-25,25)} if args.fixRangeY else {}
    #rangeY_dict = {'nEntR':(0,None), 'nEntPhi':(0,None), 'dR':(-50,50), 'dPhi':(-150,150), 'b_dR':(-50,50), 'b_dPhi':(-150,150), 'u_dR':(-50,50), 'u_dPhi':(-150,150)} if args.fixRangeY else {}
    rangeY_dict = {'nEntR':(0,None), 'nEntPhi':(0,None), 'dR':(-1*rangeR,rangeR), 'dPhi':(-1*rangePhi,rangePhi), 'b_dR':(-1*rangeR,rangeR), 'b_dPhi':(-1*rangePhi,rangePhi), 'u_dR':(-1*rangeR,rangeR), 'u_dPhi':(-1*rangePhi,rangePhi)} if args.fixRangeY else {}

    for pair in list_files:
        CompareOverlap(*makeMisDict({pair[0]: pair[1]}), outFile_name=outFile_name, rangeY_dict=rangeY_dict, isZPos=isZPos, alsoAverage=True)
    
    misTop, misBot = makeMisDict({pair[0]: pair[1] for pair in list_files})
    CompareOverlap(misTop, misBot, outFile_name=outFile_name, rangeY_dict=rangeY_dict, isZPos=isZPos, alsoAverage=True)

    
    misTop_diff = {key: misTop[key]-misTop[list_files[0][0]] for key in misTop}
    misBot_diff = {key: misBot[key]-misBot[list_files[0][0]] for key in misBot}


    #rangeY_dict = {'nEntR':(None,None), 'nEntPhi':(None,None), 'dR':(-10,10), 'dPhi':(-25,25), 'b_dR':(-10,10), 'b_dPhi':(-25,25), 'u_dR':(-10,10), 'u_dPhi':(-25,25)} if args.fixRangeY else {}
    #rangeY_dict = {'nEntR':(0,None), 'nEntPhi':(0,None), 'dR':(-50,50), 'dPhi':(-150,150), 'b_dR':(-50,50), 'b_dPhi':(-150,150), 'u_dR':(-50,50), 'u_dPhi':(-150,150)} if args.fixRangeY else {}
    rangeY_dict = {'nEntR':(0,None), 'nEntPhi':(0,None), 'dR':(-1*rangeR,rangeR), 'dPhi':(-1*rangePhi,rangePhi), 'b_dR':(-1*rangeR,rangeR), 'b_dPhi':(-1*rangePhi,rangePhi), 'u_dR':(-1*rangeR,rangeR), 'u_dPhi':(-1*rangePhi,rangePhi)} if args.fixRangeY else {}
    
    CompareOverlap(misTop_diff, misBot_diff, outFile_name=outFile_name, title='diff wrt '+list_files[0][0], rangeY_dict=rangeY_dict, isZPos=isZPos, alsoAverage=True, misTop_noDiff = misTop, misBot_noDiff = misBot)
        
      
 
    canvas.Print(outFile_name+']')
