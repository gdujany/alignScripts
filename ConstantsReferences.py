'''
This file contains the tuples containing the variations of the alignment constants:
[regex_alignables, file, [min variations], [max variations]]
'''


Velo = [
    ['Velo(Left|Right)', 'Velo/VeloGlobal.xml',  [0.002, 0.002, 0.004, 0.0000035, 0.0000035,  0.000015],
        [0.01, 0.01, 0.01, 0.000025, 0.000025,  0.00010]],
    ['Module[0-9]{2}', 'Velo/VeloModules.xml',  [0.002, 0.002, 0.004, 0.0000035, 0.0000035,  0.0001],
        [0.01, 0.01, 0.01, 0.000025, 0.000025,  0.0003]],
    ]

Tracker = [
    # IT Global
    ['(ITSystem|ITT[1-3]{1})', 'IT/ITGlobal.xml', [0.00001]*6, [0.001]*6],
    ['ITT.*Box', 'IT/ITGlobal.xml', [0.08, 0.002, 1.0, 0.00001, 0.00001, 0.002], [0.2, 0.01, 2.0, 0.001, 0.001, 0.01]],

    # IT Modules
    ['ITT.*Layer.*Ladder.*', 'IT/ITModules.xml',  [0.00001]*6, [0.001]*6],
    ['ITT.*Layer.*', 'IT/ITModules.xml',  [0.00001]*6, [0.001]*6],

    # TT Global
    ['(TTSystem|TT[a-b]{1})', 'TT/TTGlobal.xml', [0.00001]*6, [0.001]*6],
    ['TT.*Layer', 'TT/TTGlobal.xml', [0.00001, 0.00001, 0.2, 0.00001, 0.00001, 0.00001], [0.001, 0.001, 0.5, 0.001, 0.001, 0.001]],

    # TT Modules
    ['TT.*LayerR.*Module.*', 'TT/TTModules.xml',  [0.1, 0.0001, 0.0001, 0.00001, 0.00001, 0.0005], [0.2, 0.001, 0.001, 0.0001, 0.0001, 0.001]],

    # OT Global
    ['(OTSystem|T[1-3]{1})', 'OT/OTGlobal.xml', [0.00001]*6, [0.001]*6],
    ['T(1|2|3)(X1|U|V|X2)', 'OT/OTGlobal.xml', [0.001]*6, [0.002]*6],
    ['T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)', 'OT/OTGlobal.xml', [0.02, 0.0005, 0.2, 0.0005, 0.0005,0.001], [0.4, 0.002, 0.5, 0.002, 0.002, 0.01]*6],

    # OT Modules
    ['T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M(1|2)', 'OT/OTModules.xml',  [0.25, 0.0001, 0.00001, 0.00001, 0.00001, 0.001], [0.4, 0.001, 0.001, 0.001, 0.001, 0.005]],
    ['T(1|2|3)(X1|U|V|X2)Q(0|1|2|3)M[3-9]{1}', 'OT/OTModules.xml',  [0.1, 0.0001, 0.00001, 0.00001, 0.00001, 0.001], [0.3, 0.001, 0.001, 0.001, 0.001, 0.005]],
    ]
