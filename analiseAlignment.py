#!/usr/bin/env python

import sys, os, argparse, fnmatch


parser = argparse.ArgumentParser(description ="Macro to automatise the analysis of a new alignment, runs the important macros take the AlignmentResults as an input and compare Iter0 and last Iter")
parser.add_argument('directory',help='directory AlignmentResults',default=os.getcwd())
parser.add_argument('-i','--iterMax',help='maximum iteration to take', type=int)
parser.add_argument('-y','--notFixRangeY',help='do not fix y-range to default values',action='store_true')
group = parser.add_mutually_exclusive_group()
group.add_argument('-d','--database',help='database to use as initial',choices=['2011', '2012', 'survey', 'sim'],default='2012')
group.add_argument('-f','--dbFolder',help='database folder to use as initial')
parser.add_argument('-p','--plotsDir',help='create folder with a pdf for each plot',nargs='?',const='plots')
args = parser.parse_args()

# Find directory AlignResults
if fnmatch.fnmatch(args.directory, '*AlignmentResults*'):
    if args.directory[-1] != '/': args.directory+='/'
    inDir = args.directory
    outDir = args.directory.replace('AlignmentResults','SynthesisAlignment')

for root, dirs, files in os.walk(args.directory):
    for directory in dirs:
        if fnmatch.fnmatch(directory, '*AlignmentResults*'):
            if root[-1] != '/': root+='/'
            if directory[-1] != '/': directory+='/'
            inDir = root+directory
            outDir = root+directory.replace('AlignmentResults','SynthesisAlignment')
            break
    # if 'AlignmentResults' in dirs:
    #     inDir = root+'/AlignmentResults/'
    #     outDir = root+'/SynthesisAlignment/'

fixY_str = '' if args.notFixRangeY else ' -y'

try:
    os.mkdir(outDir)
except OSError:
    pass

# find max number iter
iterMax = len([i for i in os.listdir(inDir) if 'Iter' in i])-1

if args.iterMax != None: iterMax = args.iterMax

scripts_dir = '/afs/cern.ch/user/g/gdujany/LHCb/Alignment/alignScripts/' #'/afs/cern.ch/user/g/gdujany/LHCb/Alignment/align2011/newAlignment/scripts/'

#run readAlignlog.py
os.system(scripts_dir+'readAlignlog.py -f '+inDir+' > '+outDir+'summaryAlignLog.txt')

#run compareXML.py
#os.system(scripts_dir+'compareXML.py'+fixY_str+' Iter0:'+inDir+'Iter0/ Iter'+str(iterMax)+':'+inDir+'Iter'+str(iterMax)+' -o '+outDir+'plotsXML')
#os.system(scripts_dir+'compareXML.py'+fixY_str+' std:/afs/cern.ch/user/g/gdujany/LHCb/Alignment/align2011/newAlignment/std_align/  Iter'+str(iterMax-1)+':'+inDir+'Iter'+str(iterMax-1)+' -o '+outDir+'plotsXML')
#os.system(scripts_dir+'compareXML.py'+fixY_str+' std:~/LHCb/Alignment/alignMC/sim_std  Iter'+str(iterMax)+':'+inDir+'Iter'+str(iterMax)+' -o '+outDir+'plotsXML')
db_std = '~/LHCb/Alignment/alignScripts/{}_std'.format(args.database)
if args.dbFolder: db_std = args.dbFolder
plotsDir_str = '' if args.plotsDir == None else' -d '+outDir+args.plotsDir+'/plotsXML'
os.system(scripts_dir+'compareXML.py'+fixY_str+' std:'+db_std+'  Iter'+str(iterMax)+':'+inDir+'Iter'+str(iterMax)+' -o '+outDir+'plotsXML'+plotsDir_str)

# run Macro Wouter
os.system(scripts_dir+'runMacroWouter.py Iter0:'+inDir+'Iter0/histograms.root Iter'+str(iterMax)+':'+inDir+'Iter'+str(iterMax)+'/histograms.root'+' -o '+outDir+'plots_iter_0_'+str(iterMax)+'.pdf')

#run gSummaryOverlap.py
os.system(scripts_dir+'gSummaryOverlap.py'+fixY_str+' Iter0:'+inDir+'Iter0/histograms.root Iter'+str(iterMax)+':'+inDir+'Iter'+str(iterMax)+'/histograms.root'+' -o '+outDir+'plots_summaryOverlap.pdf')





