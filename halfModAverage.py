#!/usr/bin/env python

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description = 'Evaluate average of modules inside same half')
    parser.add_argument('directory',help='folder where files xml are')
    args = parser.parse_args()
##########################

import sys
sys.path.append('/afs/cern.ch/user/g/gdujany/LHCb/Alignment/newCode')
from sensorUtils import *


mm = SensMisCollection()
mm.readFromXML(findPath('Modules.xml',args.directory))
meanLeft = sum(mm[0:84:2],SensMis(0))/42.
meanRight = sum(mm[1:84:2],SensMis(0))/42.

# gg = VeloMis(args.directory)
# meanLeft = sum(gg.modMis[0:84:2],SensMis(0))/42.
# meanRight = sum(gg.modMis[1:84:2],SensMis(0))/42.

print 'mean A-Side: '
print meanLeft
print 'mean C-Side: '
print meanRight

